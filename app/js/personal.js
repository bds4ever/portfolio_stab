function voirBonjour(){
    $("#bonjour").removeClass('hide');
}

function hello(){
    $("#allo").removeClass('hide').slideToggle();
}

function masquerBonjour(){
    $("#bonjour").addClass('hide');
}

function voirLinkedin(actor){
    if(actor === 'ben'){
        document.location.href = './pages/connexion.php';
    }
}

/*****************************************
 *                                       *
 *       @u chargement des pages         *
 *                                       *
 *****************************************/

function avisSurEtudiant(){
    $('#avis-sur-etudiant-dialog').modal('show');
}

function ouvrirPageConnexion(){
    document.location.href = './pages/connexion.php';
}

function creerPortfolio(){
    // ouvrir la page de construction du portfolio
    document.location.href = 'building.php';
}

function previsualiserPortfolio(){
    // ouvrir la page de prévisualisation du portfolio
    document.location.href = 'portfolio-consultation.php';
}

function chercherEtudiant(){
    document.location.href = './pages/tableau-recherche.php';
}

function retourSurPage(source){
    if(source==='index'){
        $("#recherche-classique").removeClass('hide');
        $("#autre-mode-recherche").addClass('hide');

        $("#autre-mode").removeClass('hide');
        $("#retour").addClass('hide');
    }

    if(source==='tableauRecherche' || source==='management' || source==='a-propos-portfolio' || source==='contact' || source==='copyright' ){
        document.location.href = '../index.php';
    }

    if(source==='portfolioConsultation' || source==='building'){
        document.location.href = './homepage-student.php';
    }

    if(source==='recuperation-password'){
         document.location.href = './connexion.php';
    }

}

function autreModeRecherche(){
    $("#recherche-classique").addClass('hide');
    $("#autre-mode-recherche").show();
    $("#autre-mode").addClass('hide');
    $("#retour").show();
}

/**
 * formulaire 'etudiant'
 */

function afficherFormulaireEtudiant(){
    // Tout formulaire différent de celui de l'Etudiant reste caché!
    $("#formulaire-etudiant").removeClass('hide');
    $("#formulaire-entreprise").addClass('hide');
    $("#formulaire-enseignant").addClass('hide');

    // masquer inscription étudiant
    $("#inscription-etudiant").addClass('hide');
    //afficher par défaut
    $("#connexion-etudiant").removeClass('hide');
    // mise en évidence du bloc ciblé, au clic
    $(".arounder").css({"border" : "2px solid #00345F"});

    // mise en évidence de la 'card' cliquée
    $("#etudiant-card").css({"border" : "2px solid #00345F", "transform" : "scale(1.05)"});
    // desactivation effet des autres cards
    $("#enseignant-card").css({"border" : "none", "transform" : "none"});
    $("#entreprise-card").css({"border" : "none", "transform" : "none"});
    // couleur par défaut des boutons
    $(".btn-inscription-etudiant").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
    $(".btn-connexion-etudiant").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
}

function afficherConnexionEtudiant(){
    // masquer inscription
    $("#inscription-etudiant").addClass('hide');
    // afficher la connexion 'etudiant'
    $("#connexion-etudiant").removeClass('hide');
    // couleur par défaut des boutons
    $(".btn-connexion-etudiant").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-inscription-etudiant").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
}

function afficherInscriptionEtudiant(){
    // affichage et/ou masquage de blocs
    $("#inscription-etudiant").removeClass('hide');
    $("#connexion-etudiant").addClass('hide');
    // alternance de couleur des boutons
    $(".btn-inscription-etudiant").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-connexion-etudiant").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
}

/**
 * formulaire 'entreprise'
 */

function afficherFormulaireEntreprise(){
    //afficher par défaut
    $("#connexion-entreprise").removeClass('hide');
    //masquer par défaut
    $("#inscription-entreprise").addClass('hide');
    // afficher le formulaire de l'entreprise
    $("#formulaire-entreprise").removeClass('hide');
    // cacher les formulaires d'autres utilisateurs
    $("#formulaire-etudiant").addClass('hide');
    $("#formulaire-enseignant").addClass('hide');
    // mise en évidence du bloc ciblé, au clic
    $(".arounder").css({"border" : "2px solid #00345F"});
    // mise en évidence de la 'card' cliquée
    $("#entreprise-card").css({"border" : "2px solid #00345F", "transform" : "scale(1.05)"});

    // desactivation effet des autres cards
    $("#etudiant-card").css({"border" : "none", "transform" : "none"});
    $("#enseignant-card").css({"border" : "none", "transform" : "none"});
    // propriétés par défaut des boutons
    $(".btn-inscription-entreprise").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
    $(".btn-connexion-entreprise").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
}

function afficherInscriptionEntreprise(){
    // affichage et/ou masquage de blocs
    $("#inscription-entreprise").removeClass('hide');
    $("#connexion-entreprise").addClass('hide');
    // alternance de couleur des boutons  btn-inscription-etudiant
    $(".btn-inscription-entreprise").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-connexion-entreprise").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
}

function afficherConnexionEntreprise(){
    // affichage et/ou masquage de blocs
    $("#inscription-entreprise").addClass('hide');
    $("#connexion-entreprise").removeClass('hide');
    // alternance de couleur des boutons
    $(".btn-connexion-entreprise").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-inscription-entreprise").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
    // $(".form-control").css({"border-color" : "#00345F"});
}

/**
 * formulaire enseignant
 */

function afficherFormulaireEnseignant(){
    //afficher par défaut
    $("#connexion-enseignant").removeClass('hide');
    //masquer par défaut
    $("#inscription-enseignant").addClass('hide');
    // masquer tout formulaire différent de celui de l'Enseignant
    $("#formulaire-enseignant").removeClass('hide');
    $("#formulaire-entreprise").addClass('hide');
    $("#formulaire-etudiant").addClass('hide');
    // mise en évidence du bloc ciblé, au clic
    $(".arounder").css({"border" : "2px solid #00345F"});
    // application de la couleur par défaut du bouton préselectionné
    $(".btn-connexion-enseignant").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-inscription-enseignant").css({"background-color" : "#E0E0E0", "color" : "black", "font-weight" : "bold"});
    // mise en évidence de la 'card' cliquée
    $("#enseignant-card").css({"border" : "2px solid #00345F", "transform" : "scale(1.05)"});
    // desactivation effet des autres cards
    $("#etudiant-card").css({"border" : "none", "transform" : "none"});
    $("#entreprise-card").css({"border" : "none", "transform" : "none"});
}

function afficherInscriptionEnseignant(){
    // affichage et/ou masquage de blocs
    $("#inscription-enseignant").removeClass('hide');
    $("#connexion-enseignant").addClass('hide');
    // alternance de couleur des boutons  btn-inscription-etudiant
    $(".btn-inscription-enseignant").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-connexion-enseignant").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
}

function afficherConnexionEnseignant(){
    // affichage et/ou masquage de blocs
    $("#inscription-enseignant").addClass('hide');
    $("#connexion-enseignant").removeClass('hide');
    // alternance de couleur des boutons
    $(".btn-connexion-enseignant").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    $(".btn-inscription-enseignant").css({"background-color" : "#E0E0E0", "color" :"black", "font-weight" : "bold"});
}

/*****************************************
 *                                       *
 *            utilisateur                *
 *                                       *
 *****************************************/

function gererUtilisateur(){
    $("#gerer-utilisateur").removeClass('hide');
    // masquer les zones inutiles
    $("#gerer-mention").addClass('hide');
    
    $("#gerer-domaine-etudes").addClass('hide');
    // fermeture des insertions
    $("#inserer-mention").addClass('hide');
    $("#inserer-domaine-etudes").addClass('hide');
    $("#inserer-mention").addClass('hide');

    // fermeture des listes
    $("#liste-mention").addClass('hide');
    $("#liste-domaine-etudes").addClass('hide');
    $("#liste-mention").addClass('hide');
   
    // appliquer la couleur #28A745 au lien
    $(".parametrer-utilisateur").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});

    // appliquer la couleur #00345F aux liens non cliqués
    $(".parametrer-mention").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    $(".parametrer-domaine-etudes").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    // desactiver les propriétés activables au clic
    $(".inserer-mention").css({"color" : "#00345F"});
    // $(".inserer-niveau-etudes").css({"color" : "#00345F"});
    $(".inserer-domaine-etudes").css({"color" : "#00345F"});
    $(".inserer-nouvel-utilisateur").css({"color" : "#00345F"});

    $(".lister-mention").css({"color" : "#00345F"});
    // $(".lister-niveau-etudes").css({"color" : "#00345F"});
    $(".lister-domaine-etudes").css({"color" : "#00345F"});
    $(".lister-utilisateur").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
}

function insererNouvelUtilisateur(){
    // appliquer la couleur #28A745 au lien
    $(".inserer-nouvel-utilisateur").css({"color" : "#28A745"});
    // appliquer la couleur #00345F au lien non cliqué
    $(".lister-utilisateur").css({"color" : "#00345F"});
}

function listerUtilisateur(){
    // appliquer la couleur #28A745 au lien
    $(".lister-utilisateur").css({"color" : "#28A745"});
    // appliquer la couleur #00345F au lien non cliqué
    $(".inserer-nouvel-utilisateur").css({"color" : "#00345F"});
}

/*****************************************
 *                                       *
 *               mention                 *
 *                                       *
 *****************************************/

function gererMention(){
    $("#gerer-mention").removeClass('hide');
    // masquer les zones inutiles
    
    $("#gerer-domaine-etudes").addClass('hide');
    $("#gerer-utilisateur").addClass('hide');
    // fermeture des insertions
    $("#inserer-mention").addClass('hide');
    $("#inserer-domaine-etudes").addClass('hide');
    $("#inserer-mention").addClass('hide');

    // fermeture des listes
    $("#liste-mention").addClass('hide');
    // $("#liste-domaine-etudes").addClass('hide');
    $("#liste-mention").addClass('hide');

    
    $(".parametrer-mention").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});

    // appliquer la couleur #00345F aux liens non cliqués
    $(".parametrer-utilisateur").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    $(".parametrer-domaine-etudes").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    
    // desactiver les propriétés activables au clic
    $(".inserer-mention").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
    
    // $(".inserer-niveau-etudes").css({"color" : "#00345F"});
    $(".inserer-domaine-etudes").css({"color" : "#00345F"});
    $(".inserer-nouvel-utilisateur").css({"color" : "#00345F"});

    $(".lister-mention").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    $(".lister-domaine-etudes").css({"color" : "#00345F"});
    $(".lister-utilisateur").css({"color" : "#00345F"});

}

function insererMention(){
    $("#inserer-mention").removeClass('hide');
    // fermeture des listes
    $("#liste-mention").addClass('hide');
    $("#liste-domaine-etudes").addClass('hide');
    $("#liste-mention").addClass('hide');

    // appliquer la couleur #28A745 au lien
    $(".inserer-mention").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    // appliquer la couleur #00345F au lien non cliqué
    $(".lister-mention").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
}

function fermerInsertion(){
    $("#inserer-mention").addClass('hide');
    $("#inserer-domaine-etudes").addClass('hide');
    $("#inserer-mention").addClass('hide');
    // $("#inserer-niveau-etudes").addClass('hide');
}

function listerMention(){
    $("#liste-mention").removeClass('hide');
    // masquer les zones inutiles
    $("#inserer-mention").addClass('hide');
    $("#inserer-domaine-etudes").addClass('hide');
    $("#inserer-mention").addClass('hide');

    // appliquer la couleur #28A745 au lien
    $(".lister-mention").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    // appliquer la couleur #00345F au lien non cliqué
    $(".inserer-mention").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
}

function supprimerMention(){
    $("#mention1").remove();
}

/*****************************************
 *                                       *
 *         niveau d'études               *
 *                                       *
 *****************************************/

// function gererNiveauEtudes(){
//     $("#gerer-niveau-etudes").removeClass('hide');
//     // masquer les zones inutiles
//     $("#gerer-mention").addClass('hide');
//     $("#gerer-domaine-etudes").addClass('hide');
//     $("#gerer-utilisateur").addClass('hide');
//     // fermeture des insertions
//     $("#inserer-mention").addClass('hide');
//     $("#inserer-domaine-etudes").addClass('hide');
//     $("#inserer-mention").addClass('hide');
//     $("#inserer-niveau-etudes").addClass('hide');
//     // fermeture des listes
//     $("#liste-mention").addClass('hide');
//     $("#liste-domaine-etudes").addClass('hide');
//     $("#liste-mention").addClass('hide');
//     $("#liste-niveau-etudes").addClass('hide');
//     // appliquer la couleur #28A745 au lien
//     $(".parametrer-niveau-etudes").css({"color" : "#28A745", "font-weight":"bold"});
//     // appliquer la couleur #00345F aux liens non cliqués
//     $(".parametrer-utilisateur").css({"color" : "#00345F", "font-weight":"normal"});
//     $(".parametrer-domaine-etudes").css({"color" : "#00345F", "font-weight":"normal"});
//     $(".parametrer-mention").css({"color" : "#00345F", "font-weight":"normal"});
//     // desactiver les propriétés activables au clic
//     $(".inserer-mention").css({"color" : "#00345F"});
//     $(".inserer-niveau-etudes").css({"color" : "#00345F"});
//     $(".inserer-domaine-etudes").css({"color" : "#00345F"});
//     $(".inserer-nouvel-utilisateur").css({"color" : "#00345F"});

//     $(".lister-mention").css({"color" : "#00345F"});
//     $(".lister-niveau-etudes").css({"color" : "#00345F"});
//     $(".lister-domaine-etudes").css({"color" : "#00345F"});
//     $(".lister-utilisateur").css({"color" : "#00345F"});
// }

// function insererNiveauEtudes(){
//     $("#inserer-niveau-etudes").removeClass('hide');
//     // appliquer la couleur #28A745 au lien
//     $(".inserer-niveau-etudes").css({"color" : "#28A745"});
//     // appliquer la couleur #00345F au lien non cliqué
//     $(".lister-niveau-etudes").css({"color" : "#00345F"});
// }


// function listerNiveauEtudes(){
//     // appliquer la couleur #28A745 au lien
//     $(".lister-niveau-etudes").css({"color" : "#28A745"});
//     // appliquer la couleur #00345F au lien non cliqué
//     $(".inserer-niveau-etudes").css({"color" : "#00345F"});
// }

/*****************************************
 *                                       *
 *            domaines                   *
 *                                       *
 *****************************************/

function gererDomaineEtudes(){
    $("#gerer-domaine-etudes").removeClass('hide');
    // masquer les zones inutiles
    $("#gerer-mention").addClass('hide');
    $("#gerer-utilisateur").addClass('hide');

    // fermeture des insertions
    $("#inserer-mention").addClass('hide');
    $("#inserer-domaine-etudes").addClass('hide');
    $("#inserer-mention").addClass('hide');

    // fermeture des listes
    $("#liste-mention").addClass('hide');
    $("#liste-domaine-etudes").addClass('hide');
    $("#liste-mention").addClass('hide');

    // appliquer la couleur #28A745 au lien
    $(".parametrer-domaine-etudes").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});

    // appliquer la couleur #00345F aux liens non cliqués
    $(".parametrer-utilisateur").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    $(".parametrer-mention").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
    // desactiver les propriétés activables au clic
    $(".inserer-mention").css({"color" : "#00345F"});
 
    $(".inserer-domaine-etudes").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

    $(".inserer-nouvel-utilisateur").css({"color" : "#00345F"});

    $(".lister-mention").css({"color" : "#00345F"});

    $(".lister-domaine-etudes").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
    $(".lister-utilisateur").css({"color" : "#00345F"});
}

function insererDomaineEtudes(){
    $("#inserer-domaine-etudes").removeClass('hide');
    // fermeture des listes
    $("#liste-mention").addClass('hide');
    $("#liste-domaine-etudes").addClass('hide');
    $("#liste-mention").addClass('hide');
 
    // appliquer la couleur #28A745 au lien
    $(".inserer-domaine-etudes").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    // appliquer la couleur #00345F au lien non cliqué
    $(".lister-domaine-etudes").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});
}


function listerDomaineEtudes(){
    $("#liste-domaine-etudes").removeClass('hide');
    // masquer les zones inutiles
    $("#inserer-mention").addClass('hide');
    $("#inserer-domaine-etudes").addClass('hide');
    $("#inserer-mention").addClass('hide');

    // appliquer aux boutons
    $(".lister-domaine-etudes").css({"background-color" : "#00345F", "color" : "white", "font-weight" : "bold"});
    // appliquer la couleur #00345F au lien non cliqué
    $(".inserer-domaine-etudes").css({"color" : "#00345F", "border":"1px solid transparent",
                                     "font-weight":"normal", "box-shadow":"1px 1px 12px #555;", "background":"transparent"});

}

/****************************************************************
 *                                                              *
 *                         clonage                              *
 *                                                              *
 ****************************************************************/

function ajouterAutreDiplome(){
    $("#autre-diplome").removeClass('hide');
    // clonage du bloc d'ajout du diplome
    var button = $('#clonage-diplome').clone();
    // vidage des champs du bloc initial avant copie
    button.find('input').val('');
    button.find('textarea').val('');
    button.attr('id', 'clonage-diplome');

    button.find('input').first().attr('id', -1); //Pour stocker l'identifiant du diplome

    // coller la copie du bloc dans la div portant la classe indexée
    button.insertBefore('.clone-diplome');
}


function ajouterAutreParcours() {
    var button = $('#clonage-parcours').clone();
    button.find('input').val('');
    button.find('textarea').val('');
    button.attr('id', 'clonage-parcours');
    button.find('input').first().attr('id', -1);

    var select_domaine = button.find('#slct0 option');
    select_domaine.each(function (index) {
        if(index == 0){
            $(this).prop('selected',true);
        }else{
            $(this).prop('selected',false);
        }
    });

    var select_niveau_etudes = button.find('#slct1 option');
    select_niveau_etudes.each(function (index) {
        if(index == 0){
            $(this).prop('selected',true);
        }else{
            $(this).remove();
        }
    });

    var select_mention = button.find('#slct2 option');
    select_mention.each(function (index) {
        if(index == 0){
            $(this).prop('selected',true);
        }else{
            $(this).remove();
        }
    });

    button.insertBefore('.clone-parcours');
}

function onDomaineSelect(event) {
    var parent = $(event.target).parent().parent();
    var id = event.target.value;
    var data = {
        id_domaine: id
    };

    $.post(
        "../includes/view/select_niveau_etudes.php",
        data
    ).done(function (data) {
        var niveau_etudes = JSON.parse(data);

        var niveau_etudes_removes = parent.find('#slct1 option');
        var mention_removes = parent.find("#slct2 option");

        niveau_etudes_removes.each(function (index) {
            if(index == 0){
                $(this).prop('selected',true);
            }else{
                $(this).remove();
            }
        });

        mention_removes.each(function (index) {
            if(index == 0){
                $(this).prop('selected',true);
            }else{
                $(this).remove();
            }
        });

        var bloc_niveau_etudes = parent.find("#slct1");
        $.each(niveau_etudes, function (i, obj) {
            var option = $("<option></option>").text(obj.titre).attr('value',obj.id);
            bloc_niveau_etudes.append(option);
        })
    });
}


function onNiveauEtudesSelect(event) {
    var parent = $(event.target).parent().parent();
    var id_niveau_etude = event.target.value;
    var id_domaine = parent.find("#slct0").val();

    var data = {
        id_niveau_etude: id_niveau_etude,
        id_domaine: id_domaine
    };

    $.post(
        "../includes/view/select_mention.php",
        data
    ).done(function (data) {
        var mentions = JSON.parse(data);

        var mention_removes = parent.find("#slct2 option");
        mention_removes.each(function (index) {
            if(index == 0){
                $(this).prop('selected',true);
            }else{
                $(this).remove();
            }
        });

        var bloc_mentions = parent.find("#slct2");
        $.each(mentions, function (i, obj) {
            var option = $("<option></option>").text(obj.titre).attr('value',obj.id);
            bloc_mentions.append(option);
        })
    });
}


function ajouterAutreMention(){
    // afficher le bloc 'autre mention'
    $("#autre-mention").removeClass('hide');
    // clonage du bloc d'ajout du diplome
    var button = $('#clonage-mention').clone();
    // vidage des champs du bloc initial avant copie
    button.find('input').val('');
    button.find('textarea').val('');
    button.find('#montrerNiveauEtudes label').remove();
    button.find('#montrerNiveauEtudes select').remove();
    button.find('#montrerMention label').remove();
    button.find('#montrerMention select').remove();
    button.find('#motivation').addClass('hide');

    // button.attr('id', '');
    // coller la copie du bloc dans la div portant la classe indexée
    button.insertBefore('.clone-mention');
}

function ajouterAutreExperiencePro(){
    // clonage du bloc d'ajout du diplome
    var button = $('#clonage-experiences-professionnelles').clone();
    // vidage des champs du bloc initial avant copie
    button.find('input').val('');
    button.find('textarea').val('');
    button.attr('id', 'clonage-experiences-professionnelles');

    button.find('input').first().attr('id', -1); //Pour stocker l'identifiant du diplome

    //Select remis à zero
    var select = button.find('#statut option');
    select.each(function (index) {
        if(index == 0){
            $(this).prop('selected',true);
        }else{
            $(this).prop('selected',false);
        }
    });

    // coller la copie du bloc dans la div portant la classe indexée
    button.insertBefore('.clone-experience-professionelle');
}


function ajouterAutreExperienceExtra(){
    // clonage du bloc d'ajout du diplome
    var button = $('#clonage-experience-extra').clone();
    // vidage des champs du bloc initial avant copie
    button.find('input').val('');
    button.find('textarea').val('');
    button.attr('id', 'clonage-experience-extra');

    button.find('input').first().attr('id', -1); //Pour stocker l'identifiant du diplome

    // coller la copie du bloc dans la div portant la classe indexée
    button.insertBefore('.clone-experience-extra');
}

/****************************************************************
 *                                                              *
 *  gestion affichage successive dans la création du portfolio  *
 *                                                              *
 ****************************************************************/


function voirNiveauEtudes(event){

    var parent = $(event.target).parent().parent();
    var bloc_niveau_etude = $(parent).find("#montrerNiveauEtudes")[0];
    var bloc_mention = $(parent).find("#montrerMention")[0];
    var bloc_motivation = $(parent).find("#motivation")[0];

    if( $(event.target).is(':selected') ){
        $(bloc_niveau_etude).fadeOut();
    }
    else {
        //var nb = $('option[value="selectionDomaine"]').length;
        var domaineid = event.target.value;

        $.ajax({
            url : '../includes/view/select_niveau_etudes.php',
            type : 'POST',
            data : 'id_domaine=' + parseInt(domaineid),
            dataType : 'html',
            success : function(code_html, statut){
                $(bloc_niveau_etude).html(code_html).fadeIn();
            },
            error : function(resultat, statut, erreur){
                alert(erreur);
            }
        });


        //var bloc_mention = $('#clonage-mention');
        //console.log(bloc_mention);

        $(bloc_mention).html("").fadeOut();
        $(bloc_motivation).fadeOut();

        //$("#montrerNiveauEtudes").fadeIn();
    }
}

function voirMention(event){
    var parent = $(event.target).parent().parent();
    var bloc_mention = $(parent).find("#montrerMention")[0];
    var bloc_motivation = $(parent).find("#motivation")[0];

    if( $(event.target).is(':selected') ){
        $(bloc_mention).fadeOut();
    }
    else {
        var domaineid = $(parent).find('#slct0')[0].value;
        var niveauid = $(parent).find('#slct1')[0].value;

        $.ajax({
            url : '../includes/view/select_mention.php',
            type : 'POST',
            data : 'id_domaine=' + parseInt(domaineid) + '&id_niveau_etude=' + parseInt(niveauid),
            dataType : 'html',
            success : function(code_html, statut){
                $(bloc_mention).html(code_html).fadeIn();
            },
            error : function(resultat, statut, erreur){
                alert(erreur);
            }
        });

        $(bloc_motivation).fadeOut();
        //$("#montrerMention").fadeIn();
    }
}

function montrerMotivation(event){
    var parent = $(event.target).parent().parent();
    var bloc_motivation = $(parent).find("#motivation")[0];

    if( $(event.target).is(':selected') ){
        $(bloc_motivation).fadeOut();
    }
    else {
        $(bloc_motivation).fadeIn();
    }
}

function supprimerDiplomeClone(){
    // $('#clonage-diplome:last').remove();
    // $('#clonage-diplome:not(first-child)').remove();
}


/**
 * end
 */

function slideProfessionalExp(){
    $("#proExp").removeClass('hide').slideToggle();
    $("#ico-experience-pro").toggleClass("fa-chevron-down fa-chevron-right");
}

function slideExperienceXtraPro(){
    $("#experienceXtraPro").removeClass('hide').slideToggle();
    $("#ico-experience-etxtra-pro").toggleClass("fa-chevron-down fa-chevron-right");
}

function slideProjetPro(){
    $("#projetPro").addClass('hide');
    // $("#ico-projet-pro").toggleClass("fa-chevron-down fa-chevron-right");
}

function slideStatus(){
     $("#open").removeClass('hide');
}

function slideParcours(){
    $("#parcours").removeClass('hide').slideToggle();
    // $(".ico").toggleClass("fa-minus fa-plus"); ok
    $("#ico-parcours-acad").toggleClass("fa-chevron-down fa-chevron-right");

}

function slideReference() {
    $("#reference").slideToggle();
}

function slideCompetence(){
    $("#competence").slideToggle();
    $("#ico-competence").toggleClass("fa-chevron-down fa-chevron-right");
}

function slideDiplome(){
    $("#diplome").slideToggle();
    $("#ico-diplome").toggleClass("fa-chevron-down fa-chevron-right");
}

function slideContact(){
    $("#contact").slideToggle();
    $("#ico-contact").toggleClass("fa-chevron-down fa-chevron-right");
}

function slideLangues() {
    $("#langues").slideToggle();
    $("#ico-connaissance-ling").toggleClass("fa-chevron-down fa-chevron-right");
}

function openSimpleSearch(){
    $("#advanced-search").addClass('hide');
    $(".classic-search").removeClass('hide');
    $(".btn-open-simple-search").addClass('hide');
    $(".btn-open-adv-search").removeClass('hide');
}

function openAdvancedSearch(){
    $("#advanced-search").removeClass('hide');
    $(".classic-search").addClass('hide');
    $(".btn-open-adv-search").addClass('hide');
    $(".btn-open-simple-search").removeClass('hide');
}


// function remplir(s0,s1,s2){
//     var s0 = document.getElementById(s0);
//     var s1 = document.getElementById(s1);
//     var s2 = document.getElementById(s2);
//     s2.innerHTML = "";
//     if((s0.value == "artLettreLangue") & (s1.value == "licence")){
//         var optionArray = ["selectionMention|Sélectionnez","philosophie|Philosophie","sciences_sociales|Sciences sociales","sciences_éducation|Sciences de l'éducation","histoire|Histoire","géographie_aménagement|Géographie et aménagement"];
//     } else if(s1.value == "licencePro"){
//         var optionArray = ["selectionMention|Sélectionnez","techniques_son_image|Techniques du son et de l'image","métiers_livre|Métiers du livre: documentation et bibliothèque","métiers_communication|Métiers de la communication: chargé de communication"];
//     } else if(s1.value == "master"){
//         var optionArray = ["selectionMention|Sélectionnez","arts_scène|Arts de la scène et du spectacle vivant","arts_plastiques|Arts plastiques","cinémas_audiovisuel|Cinémas et audiovisuel","histoire_art|Histoire de l'art","lettres|Lettres"];
//     }
//     for(var option in optionArray){
//         var pair = optionArray[option].split("|");
//         var newOption = document.createElement("option");
//         newOption.value = pair[0];
//         newOption.innerHTML = pair[1];
//         s2.options.add(newOption);
//     }
// }

function voirInformation(action){
    if(action==='niveauExpertise'){
        document.location.href = '../index.php';
    }
}

// function ouvrirContact(location){
//     if(location === 'index'){
//         document.location.href = './pages/contact.php';
//     }

//     else{
//         document.location.href = 'contact.php';
//     }
// }

function ouvrirCopyright(location){
    if(location === 'index'){
        document.location.href = './pages/copyright.php';
    }

    else{
        document.location.href = 'copyright.php';
    }
}

function ouvrirRecuperationPassword(){
    document.location.href = 'recuperation-password.php';
}

// function ouvrirRecuperationPassword(acteur){

//     if(acteur==='etudiant'){
//         // masquer la page active
//         $("#formulaire-etudiant").fadeOut();
//         // afficher la fenêtre de récupération de mot de passe
//         $("#password-recuperation").fadeIn();
//     }

//     if(acteur==='enseignant'){
//         // masquer la page active
//         $("#formulaire-enseignant").fadeOut();
//         // afficher la fenêtre de récupération de mot de passe
//         $("#password-recuperation").fadeIn();
//     }

//     if(acteur==='entreprise'){
//         // masquer la page active
//         $("#formulaire-entreprise").fadeOut();
//         // afficher la fenêtre de récupération de mot de passe
//         $("#password-recuperation").fadeIn();
//     }
// }

function ouvrirApropos(location){
    if(location === 'index'){
        document.location.href = './pages/a-propos-portfolio.php';
    }

    else{
        document.location.href = 'a-propos-portfolio.php';
    }
}

function voirFormulaireContact(){
    // $("#formulaire-contact").show().css({"margin-bottom" : "2em"});
    $("#formulaire-contact").removeClass('hide').css({"margin-bottom" : "2em"});
}

function selectionnerPassion(){
    if( $('option[value=selectionPassion]').is(':selected') ){
        $(".description-experience").fadeOut();
    } else if($('option[value=passionAutre]').is(':selected') ){
            $("#autre-experience").fadeIn();
        }
    else {
        $(".description-experience").fadeIn();
        $("#autre-experience").fadeOut();
    }
}

function fermer(location){
    if(location === 'connexion'){
        $("#password-recuperation").fadeOut();
    }
}

/****************************************************************
 *                                                              *
 *    backgrounf mobile au hover sur la page de consultation    *
 *                                                              *
 ****************************************************************/

function allerVersAncre(ancre){
    // projet pro
    if (ancre==='projetProfessionnel'){
        // activation projet pro
        $("#change-pro-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences pro
        $("#change-experience-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences extra pro
        $("#change-experience-extra-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation références
        $("#change-references-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation langues
        $("#change-connaissances-ling-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation contact
        $("#change-contact-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }
    // parcours acad
    if(ancre==='parcoursAcademique'){
       // activation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation projet pro
        $("#change-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences pro
        $("#change-experience-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences extra pro
        $("#change-experience-extra-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation références
        $("#change-references-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation langues
        $("#change-connaissances-ling-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation contact
        $("#change-contact-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }

    // expériences pro
    if(ancre==='experiencePro'){
        // activation experience pro
        $("#change-experience-pro-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation projet pro
        $("#change-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences extra pro
        $("#change-experience-extra-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation références
        $("#change-references-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation langues
        $("#change-connaissances-ling-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation contact
        $("#change-contact-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }

    // expériences extra-propro
    if(ancre==='experienceExtraPro'){
        // activation expériences extra-propro
        $("#change-experience-extra-pro-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation projet pro
        $("#change-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences pro
        $("#change-experience-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation références
        $("#change-references-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation langues
        $("#change-connaissances-ling-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation contact
        $("#change-contact-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }

    // références
    if(ancre==='references'){
        $("#change-references-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation projet pro
        $("#change-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences pro
        $("#change-experience-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences extra pro
        $("#change-experience-extra-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation langues
        $("#change-connaissances-ling-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation contact
        $("#change-contact-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }

    // références
    if(ancre==='connaissanceLing'){
        $("#change-connaissances-ling-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation projet pro
        $("#change-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences pro
        $("#change-experience-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences extra pro
        $("#change-experience-extra-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation contact
        $("#change-contact-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation références
        $("#change-references-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }

    // références
    if(ancre==='contact'){
        $("#change-contact-property").addClass('flash-consultation-active').removeClass('flash-consultation');
        // desactivation projet pro
        $("#change-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation parcours acad
        $("#change-parcours-cademique-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences pro
        $("#change-experience-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation expériences extra pro
        $("#change-experience-extra-pro-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation langues
        $("#change-connaissances-ling-property").addClass('flash-consultation').removeClass('flash-consultation-active');
        // desactivation références
        $("#change-references-property").addClass('flash-consultation').removeClass('flash-consultation-active');
    }
}

function fermerInsertionDomaine(){
    $("#liste-domaine-etudes").addClass('hide');
}

/****************************************************************
 *                                                              *
 *                   chargement de la photo de l'utilisateur    *
 *                                                              *
 ****************************************************************/

// function loadImage(path, width, height, target) {
//     $('<img src="'+ path +'">').load(function() {
//       $(this).width(width).height(height).appendTo(target);
//     });
// }

// function loadImage(path, width, height, target) {
//     $('<img src="'img/decideur.jpg'">').load(function() {
//       $(this).width(300).height(300).appendTo(.heyImg);
//     });
// }



function clone(){
    var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;
    $(this).parents(".clonedInput").clone()
        .appendTo("body")
        .attr("id", "clonedInput" +  cloneIndex)
        .find("*")
        .each(function() {
            var id = this.id || "";
            var match = id.match(regex) || [];
            if (match.length == 3) {
                this.id = match[1] + (cloneIndex);
            }
        })
    cloneIndex++;
}


function remove(){
    $(this).parents(".clonedInput").remove();
}



function connexionVoirPortfolio(){
    // document.location.href = './modal/connexion-voir-portfolio.php';
    $('.connexion-voir-portfolio').modal('show');
}

function voirBtnChgAdmin(){
    // si cocher, afficher colonne et action indexées
    // if( $('input[name=case_admin]').is(':checked') )
    //     $('input[name=case_admin]').attr('checked'){
    //     $("#voirColAction").show();
    //     $("#voirBtnAdminConfig").show();
    // }

    if( $('.nonon').is(':checked') )
        {
        $("#voirColAction").show();
        $(".voirBtnAdminConfig").show();
    }
}


// $('input[name=foo]').is(':checked')
// $('input[name=foo]').attr('checked')

function showPassword(utilisateur){
    // if(utilisateur==='etudiant'){
    //     $(#pwd-etudiant-saisi);
    // }
}

function reduireNav(){
    $(".tailleNav").removeClass("flash-consultation");

}

/****************************************************************
 *                                                              *
 *                   consultation de portfolio                  *
 *                                                              *
 ****************************************************************/

function loadConsultationPortfolio(){
    $(".consultation-btn-profil").css({"color" : "white", "border-bottom":"2px solid white", "border-top":"2px solid white", "padding":"5px"});
    // inistialisation des propriétés des autres boutons
    $(".consultation-btn-contact").css({"color" : "white", "border":"none"});
    $(".consultation-btn-parcours").css({"color" : "white", "border":"none"});
    $(".consultation-btn-competence").css({"color" : "white", "border":"none"});
}

function loadContactPortfolio(){
    $(".consultation-btn-contact").css({"color" : "white", "border-bottom":"2px solid white", "border-top":"2px solid white", "padding":"5px"});
    // inistialisation des propriétés des autres boutons
    $(".consultation-btn-profil").css({"color" : "white", "border":"none"});
    $(".consultation-btn-parcours").css({"color" : "white", "border":"none"});
    $(".consultation-btn-competence").css({"color" : "white", "border":"none"});
}

function consulerBtnProfil(){
    $(".consultation-btn-profil").css({"color" : "white", "border-bottom":"2px solid white", "border-top":"2px solid white", "padding":"5px"});
    // inistialisation des propriétés des autres boutons
    $(".consultation-btn-contact").css({"color" : "white", "border":"none"});
    $(".consultation-btn-parcours").css({"color" : "white", "border":"none"});
    $(".consultation-btn-competence").css({"color" : "white", "border":"none"});
}

function consulerBtnParcours(){
    $(".consultation-btn-parcours").css({"color" : "white", "border-bottom":"2px solid white", "border-top":"2px solid white", "padding":"5px"});
    // inistialisation des propriétés des autres boutons
    $(".consultation-btn-contact").css({"color" : "white", "border":"none"});
    $(".consultation-btn-profil").css({"color" : "white", "border":"none"});
    $(".consultation-btn-competence").css({"color" : "white", "border":"none"});
}

function consulerBtnCompetences(){
    $(".consultation-btn-competence").css({"color" : "white", "border-bottom":"2px solid white", "border-top":"2px solid white", "padding":"5px"});
    // inistialisation des propriétés des autres boutons
    $(".consultation-btn-contact").css({"color" : "white", "border":"none"});
    $(".consultation-btn-profil").css({"color" : "white", "border":"none"});
    $(".consultation-btn-parcours").css({"color" : "white", "border":"none"});
}

function consulerBtnContact(){
    $(".consultation-btn-contact").css({"color" : "white", "border-bottom":"2px solid white", "border-top":"2px solid white", "padding":"5px"});
    // inistialisation des propriétés des autres boutons
    $(".consultation-btn-competence").css({"color" : "white", "border":"none"});
    $(".consultation-btn-profil").css({"color" : "white", "border":"none"});
    $(".consultation-btn-parcours").css({"color" : "white", "border":"none"});

    // voir formulaire de contact
    $("#m-envoyer-mail").show();

}
