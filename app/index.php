<?php
session_start();
?>

<!DOCTYPE html>
<!-- ------------------------------------------------
 -                                                  -
 -           recherche de profil                    -
 -                                                  -
 ---------------------------------------------------->
    <?php 
        if (!empty($_POST['lancerRecherche'])) {
            $nom = $_POST['recherchenom'];
            $prenom = $_POST['rechercheprenom'];
            header("Location: pages/tableau-recherche.php?nom=".$nom."&prenom=".$prenom);
        }
    ?>
<!-- ------------------------------------------------
 -                                                  -
 -           Connexion utilisateur                  -
 -                                                  -
 ---------------------------------------------------->
    <?php
        
        try
{
$bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
//pour afficher les erreurs liées à pdo
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}
        /**************************************************
         *                                                *
         *      formulaire d'inscription etudiant         *
         *                                                *
         **************************************************/
        if(isset($_POST['forminscription_etu'])) {
           $nom = htmlspecialchars($_POST['nom']);
           $prenom = htmlspecialchars($_POST['prenom']);
           $raisonsocial = ""; //vide car pas besoin pour etudiant
           $tel = ""; //vide car pas besoin pour etudiant
           $mail = htmlspecialchars($_POST['mail']);
           $mdp = sha1($_POST['mdp']);
           $mdp2 = sha1($_POST['mdp2']);
           $role = "etu";
           $privilege = "";
           if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['mail']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2'])) {

                    if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                       $reqmail = $bdd->prepare("SELECT * FROM utilisateurs WHERE email = ?");
                       $reqmail->execute(array($mail));
                       $mailexist = $reqmail->rowCount();
                       if($mailexist == 0) {
                          if($mdp == $mdp2) {
                             $insertentudiant = $bdd->prepare("INSERT INTO utilisateurs(role, nom, prenom, raisonsocial, tel, email, motdepasse, privilege) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                             $insertentudiant->execute(array($role, $nom, $prenom, $raisonsocial, $tel, $mail, $mdp, $privilege));
                             $erreur_inscrption = "Votre compte a bien été créé !";
                          } else {
                             $erreur_inscrption = "Vos mots de passes ne correspondent pas !";
                          }
                       } else {
                          $erreur_inscrption = "Adresse mail déjà utilisée !";
                       }
                    } else {
                       $erreur_inscrption = "Votre adresse mail n'est pas valide !";
                    }

           } else {
              $erreur_inscrption = "Tous les champs marqué d'un asterisque doivent être complétés !";
           }
        }
        /**************************************************
         *                                                *
         *     formulaire d'inscription enseignant        *
         *                                                *
         **************************************************/
        if(isset($_POST['forminscription_ens'])) {
           $nom = htmlspecialchars($_POST['nom']);
           $prenom = htmlspecialchars($_POST['prenom']);
           //tel & raison social vides car pas besoin pour 'enseignant'
           $raisonsocial = ""; 
           $tel = "";
           $mail = htmlspecialchars($_POST['mail']);
           $mdp = sha1($_POST['mdp']);
           $mdp2 = sha1($_POST['mdp2']);
           $role = "ens";
           $privilege = "";
           if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['mail']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2'])) {
                    if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                       $reqmail = $bdd->prepare("SELECT * FROM utilisateurs WHERE email = ?");
                       $reqmail->execute(array($mail));
                       $mailexist = $reqmail->rowCount();
                       if($mailexist == 0) {
                          if($mdp == $mdp2) {
                             $insertenseignant = $bdd->prepare("INSERT INTO utilisateurs(role, nom, prenom, raisonsocial, tel, email, motdepasse, privilege) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                             $insertenseignant->execute(array($role, $nom, $prenom, $raisonsocial, $tel, $mail, $mdp, $privilege));
                             $erreur_inscrption = "Votre compte a bien été créé !";
                          } else {
                             $erreur_inscrption = "Vos mots de passes ne correspondent pas !";
                          }
                       } else {
                          $erreur_inscrption = "Adresse mail déjà utilisée !";
                       }
                    } else {
                       $erreur_inscrption = "Votre adresse mail n'est pas valide !";
                    }

           } else {
              $erreur_inscrption = "Tous les champs marqués d'un astérisque doivent être complétés !";
           }
        }
        /**************************************************
         *                                                *
         *     formulaire d'inscription entreprise        *
         *                                                *
         **************************************************/
        if(isset($_POST['forminscription_ent'])) {
           $nom = htmlspecialchars($_POST['nom']);
           $prenom = htmlspecialchars($_POST['prenom']);
           $raisonsocial = htmlspecialchars($_POST['raisonsocial']);
           $tel = htmlspecialchars($_POST['tel']);
           $mail = htmlspecialchars($_POST['mail']);
           $mdp = sha1($_POST['mdp']);
           $mdp2 = sha1($_POST['mdp2']);
           $role = "ent";
           $privilege = "";
           if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['raisonsocial']) AND !empty($_POST['mail']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2'])) {
                    if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                       $reqmail = $bdd->prepare("SELECT * FROM utilisateurs WHERE email = ?");
                       $reqmail->execute(array($mail));
                       $mailexist = $reqmail->rowCount();
                       if($mailexist == 0) {
                          if($mdp == $mdp2) {
                             $insertentreprise = $bdd->prepare("INSERT INTO utilisateurs(role, nom, prenom, raisonsocial, tel, email, motdepasse, privilege) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                             $insertentreprise->execute(array($role, $nom, $prenom, $raisonsocial, $tel, $mail, $mdp, $privilege));
                             $erreur_inscrption = "Votre compte a bien été créé !";
                          } else {
                             $erreur_inscrption = "Vos mots de passes ne correspondent pas !";
                          }
                       } else {
                          $erreur_inscrption = "Adresse mail déjà utilisée !";
                       }
                    } else {
                       $erreur_inscrption = "Votre adresse mail n'est pas valide !";
                    }

           } else {
              $erreur_inscrption = "Tous les champs marqué d'un asterisque doivent être complétés !";
           }
        }
        /***************************************************************************************************
         *                                                                                                 *
         *  si il y a envoie de donnée via le formulaire de co, alors on applique filtre de securité       *
         *  sur le email et mdp et on test si les ces champs ne sont pas vide sinon erreur, si les champs  *
         *  ne sont pas vide on test si ce qui a été entré est présent dans la bdd (a laide de rowCount()  *
         *  qui compte le nombre de ligne présent avec les données saisie)                                 *
         *                                                                                                 *
         ***************************************************************************************************/
        if (isset($_POST['formconnexion'])) {     
            $mailconnect = htmlspecialchars($_POST['mailconnect']);
            $mdpconnect = sha1($_POST['mdpconnect']);
            if (!empty($mailconnect) AND !empty($mdpconnect))
            {
                $requser = $bdd->prepare("SELECT * FROM utilisateurs WHERE email = ? AND motdepasse = ?");
                $requser->execute(array($mailconnect, $mdpconnect));
                $userexist = $requser->rowCount();
                if ($userexist == 1) {
                    //a supprimer
                    $erreur_co = "Connexion réussie !";
                    /*on récupere les données de chaques etudiants et on les mets dans des variables de session*/
                    $userinfo = $requser->fetch();
                    $_SESSION['id'] = $userinfo->id;
                    $_SESSION['role'] = $userinfo->role;
                    $_SESSION['nom'] = $userinfo->nom;
                    $_SESSION['prenom'] = $userinfo->prenom;
                    $_SESSION['email'] = $userinfo->email;
                    //selon son role chaque utilisateur sera redirigé vers la page bonne page
                    if ($userinfo->role == "etu") {
                        header("Location: pages/homepage-student.php?id=".$_SESSION['id']);
                    }
                    elseif ($userinfo->role == "ens"){
                        header("Location: pages/accueil-enseignant.php?id=".$_SESSION['id']);
                    }
                    else
                    {
                        header("Location: pages/homepage-company.php?id=".$_SESSION['id']);
                    }           
                }
                else{
                    $erreur_co = "Email ou mot de passe incorrecte";
                }
            }
            else
            {
                $erreur_co = "Tous les champs doivent être complétés !";
            }
        }
    ?>
<!-- ------------------------------------------------
 -                                                  -
 -           fin traitement d'accès                 -
 -                                                  -
 ---------------------------------------------------->

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="./lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="./lib/animate.css/animate.css" rel="stylesheet">
        <link href="./lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="./lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="./lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="./lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="./lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="./lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="./css/import/style.css" rel="stylesheet">
        <link href="./css/personal.css" rel="stylesheet">

    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">

        <main>
            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="./img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="#totop">Accueil</a></li>
                            <li><a class="section-scroll" href="#retrouver-profil">Retrouver un profil</a></li> 
                            <li><a class="section-scroll" href="#connexion-digifolio">Connexion</a></li>
                            
                                

                            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Plus</a>
                            <ul class="dropdown-menu">
                                <li><a class="section-scroll" href="#digifolio">A propos</a></li>
                                <li><a class="section-scroll" href="#contacter-univ">Contact</a></li>
                                <li><a href="#ancre_equipe_projet" class="section-scroll">Equipe projet</a></li>
                            </ul>                       
                        </ul>

                        <ul class="nav navbar-nav navbar-right">                        
                            <li>
                                <a class="" href="https://www.u-picardie.fr" target="_blank" title="visiter le site de l'université">Université de Picardie Jules Verne</a>
                            </li>                            
                        </ul>                  
                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;./img/slide/slide1.jpg&quot;);">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">Alimentez votre curiosité</div>
                                    <div class="font-alt mb-40 titan-title-size-4">Découvrez nos profils</div><a class="section-scroll btn btn-border-w btn-round" href="#retrouver-profil">Rechercher</a>
                                </div>
                            </div>
                            
                          
                        </li>
                        
                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;./img/slide/slide2.jpg&quot;);">
                          <div class="titan-caption">
                            <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                              <div class="font-alt mb-30 titan-title-size-1">Portfolio digital</div>
                              <div class="font-alt mb-40 titan-title-size-3">Une application sur mesure</div><a class="section-scroll btn btn-border-w btn-round" href="#digifolio">A propos</a>
                            </div>
                          </div>
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <section class="module-medium" id="retrouver-profil">
                    <?php include './includes/view/retrouver_un_profil.php' ;?>
                </section>

                <!-- connexion -->
                <section class="module-medium" id="connexion-digifolio" style="background: #fafafa;">
                    <?php include './includes/view/connexion.php';?>
                </section>
         
                <!-- section à propos du digifolio-->
                <section class="module-medium" id="digifolio">            
                    <?php include './includes/view/a_propos.php';?>
                </section>

                <!-- section à propos du digifolio-->
                <section class="module-medium" id="ancre_equipe_projet" style="background: #fafafa;">            
                    <?php include './includes/view/equipe_projet.php';?>
                </section>

                <!-- contacter l'université -->
                <section class="module-medium" id="contacter-univ">
                    <?php include './includes/view/nous_contacter.php';?>
                </section>

                <!-- <section id="map-section">
                    <div id="map"></div>
                </section> -->
                <a href="./pages/tableau-recherche.php">tableau</a>
                <!-- pied de page du site -->
                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
        </main>
        
        <!-- import des différentes librairies javascript -->
        <!-- <script src="./lib/jquery/dist/jquery.js"></script> -->

        <!-- <script src="./js/jquery-3.3.1.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="./js/import/main.js"></script>
        <script src="./lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./lib/wow/dist/wow.js"></script>
        <script src="./lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="./lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="./lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="./lib/flexslider/jquery.flexslider.js"></script>
        <script src="./lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="./lib/smoothscroll.js"></script>
        <script src="./lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="./lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="./js/import/plugins.js"></script>
        <script src="./js/personal.js"></script>
    
    </body>
</html>