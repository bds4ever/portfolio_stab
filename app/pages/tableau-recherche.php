<?php 

/*connexion a la base de donnée*/
try
{
$bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
//pour afficher les erreurs liées à pdo
    
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}


      //alors on recupere ce qui se trouve dans la barre de texte
      $rechercheParNom = $_GET['nom'];
      $rechercheParPrenom = $_GET['prenom'];
      
      
      //cas ou le champs nom est renseigné
      if(!empty($rechercheParNom) AND empty($rechercheParPrenom)){
          $reqrecherche = "SELECT * FROM `utilisateurs` WHERE role = 'etu' AND `nom` LIKE '%".$rechercheParNom."%'";
          $reqcompte = "SELECT COUNT(*) AS total FROM `utilisateurs` WHERE role = 'etu' AND `nom` LIKE '%".$rechercheParNom."%'";
        }
        //cas ou le champs prenom est renseigné
        else if(!empty($rechercheParPrenom) AND empty($rechercheParNom)){
          $reqrecherche = "SELECT * FROM `utilisateurs` WHERE role = 'etu' AND `prenom` LIKE '%".$rechercheParPrenom."%'";
          $reqcompte = "SELECT COUNT(*) AS total FROM `utilisateurs` WHERE role = 'etu' AND `prenom` LIKE '%".$rechercheParPrenom."%'";
        }
        //cas ou les deux champs sont renseigné
        else if(!empty($rechercheParPrenom) AND !empty($rechercheParNom)){
          $reqrecherche = "SELECT * FROM `utilisateurs` WHERE role = 'etu' AND (CONCAT(`nom`, `prenom`) LIKE '%".$rechercheParPrenom."%' OR CONCAT(`nom`, `prenom`) LIKE '%".$rechercheParNom."%')";
          $reqcompte = "SELECT COUNT(*) AS total FROM `utilisateurs` WHERE role = 'etu' AND (CONCAT(`nom`, `prenom`) LIKE '%".$rechercheParPrenom."%' OR CONCAT(`nom`, `prenom`) LIKE '%".$rechercheParNom."%')";
        }

         //cas ou les deux champs sont vide
        else{
          $reqrecherche = "SELECT * FROM utilisateurs WHERE role = 'etu'";
          $reqcompte = "SELECT COUNT(*) AS total FROM utilisateurs WHERE role = 'etu'";
        }


        $req_cv_utilisateurs = $bdd->prepare("SELECT c.* FROM cv_utilisateurs c INNER JOIN utilisateurs u ON u.id = c.user_id");
        $req_cv_utilisateurs->execute();



  //*************** pagination *******************

  $ligneParPage=6; //nombres de lignes par page.

  $retour_total= $bdd->prepare($reqcompte); // on prepare la requete : qui consite a compter le nombre de page
  $retour_total->execute(); // on execute la requete
  $donnees_total= $retour_total->fetch(); //on recupere la ligne qui contient le nombre total de page 

  
  $nombreDePages=ceil($donnees_total['total']/$ligneParPage); //on divise le nombre total de pages par le nombre de ligne par page et on arrondie au superieur grace a à la fonction ceil()

  if(isset($_GET['page'])) // Si la variable $_GET['page'] existe (dans l'url), isset permet de tester l'existance d'une variable
  {

    $pageActuelle=intval($_GET['page']); //transforme la valeur en parametre en valeur de type entier 

    if($pageActuelle>$nombreDePages) //si le numéro de page contenu dans $_GET['page'] est supérieur au nombre total de pages ($nombreDePage), 
    {
      $pageActuelle=$nombreDePages; //alors la page sera la page maximum
    }
  }
  else // Sinon si $_GET['page'] n'existe pas (si on a pas une url du style listemembre.php?page=[n° de la page]) la page actuelle est 1
  {
    $pageActuelle=1; // La page actuelle est la n°1    
  }

  $premiereEntree=($pageActuelle-1)*$ligneParPage; // On calcul la première entrée à lire

  // La requête sql pour récupérer les lignes de tableau de la page actuelle.
  $retour_ligne = $bdd->prepare($reqrecherche.' ORDER BY id DESC LIMIT '.$premiereEntree.', '.$ligneParPage.'');
  $retour_ligne->execute();
  
  //******************* fin pagination *********************

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="../lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../lib/animate.css/animate.css" rel="stylesheet">
        <link href="../lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="../lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="../lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="../css/import/style.css" rel="stylesheet">
        <link href="../css/personal.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">

        <main>
            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="../img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="#totop">Accueil</a></li>
                            <li><a class="section-scroll" href="../index.php#retrouver-profil">Retrouver un profil</a></li> 
                            <li><a class="section-scroll" href="../index.php#connexion-digifolio">Connexion</a></li>
                            
                                

                            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Plus</a>
                            <ul class="dropdown-menu">
                                <li><a class="section-scroll" href="../index.php#digifolio">A propos</a></li>
                                <li><a class="section-scroll" href="../index.php#contacter-univ">Contact</a></li>
                                <li><a href="../index.php#ancre_equipe_projet" class="section-scroll">Equipe projet</a></li>
                            </ul>                       
                        </ul>

                        <ul class="nav navbar-nav navbar-right">                        
                            <li>
                                <a class="" href="https://www.u-picardie.fr" target="_blank" title="visiter le site de l'université">Université de Picardie Jules Verne</a>
                            </li>                            
                        </ul>                  
                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;../img/break.jpg&quot;);">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">Découvrez nos différents profils</div>
                                    <div class="font-alt mb-40 titan-title-size-4">Profils</div><a class="section-scroll btn btn-border-w btn-round" href="#ancre_resultat_recherche_profil">Consulter</a>
                                </div>
                            </div>                        
                        </li>

                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;../img/break_2.jpg&quot;);">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">Découvrez nos différents profils</div>
                                    <div class="font-alt mb-40 titan-title-size-4">Profils</div><a class="section-scroll btn btn-border-w btn-round" href="#ancre_resultat_recherche_profil">Consulter</a>
                                </div>
                            </div>                        
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <!-- tableau -->
                <section class="module" id="ancre_resultat_recherche_profil">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <div class="form-group text-center">
                                    <input type="text" name="filtre" class="form-control dark" placeholder="filter suivant les champs" id="searchInput" value="test">
                                            
                                </div>
                            </div>
                        </div>

                        <div class="row mt-40">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="table-responsive-md">
                                    <table class="table bg-white table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nom</th>
                                                <th scope="col">Prénom</th>

                                                <th scope="col">Mention</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="fbody">
                                            <?php while (($etudinfo = $retour_ligne->fetch()) OR ($cv_utilisateur = $req_cv_utilisateurs->fetch())){ ?>
                                            <tr>
                                                <td class="text-left"> <?php echo $etudinfo['nom'] ?> </td>
                                                <td> <?php echo $etudinfo['prenom'] ?> </td>
                                                <td></td>
                                                <td class="text-left"><i class="fa fa-download"></i>Télécharger le cv de <?php echo $etudinfo['prenom']; ?>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- pied de page du site -->
                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up" style="top: 45%"><a href="#totop"><i class="fa fa-angle-double-up fa-2x"></i></a></div>

            <script type="text/javascript">
                $("#searchInput").keyup(function () {
                    //split the current value of searchInput
                    var data = this.value.split(" ");
                    //create a jquery object of the rows
                    var jo = $("#fbody").find("tr");
                    if (this.value == "") {
                        jo.show();
                        return;
                    }
                    //hide all the rows
                    jo.hide();

                    //Recusively filter the jquery object to get results.
                    jo.filter(function (i, v) {
                        var $t = $(this);
                        for (var d = 0; d < data.length; ++d) {
                            if ($t.is(":contains('" + data[d] + "')")) {
                                return true;
                            }
                        }
                        return false;
                    })
                    //show the rows that match.
                    .show();
                }).focus(function () {
                    this.value = "";
                    $(this).css({
                        "color": "black"
                    });
                    $(this).unbind('focus');
                }).css({
                    "color": "#C0C0C0"
                });
            </script>
        </main>
        
        <!-- import des différentes librairies javascript -->
        <!-- <script src="./lib/jquery/dist/jquery.js"></script> -->

        <!-- <script src="./js/jquery-3.3.1.js"></script> -->
        
        <script src="../js/import/main.js"></script>
        <script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../lib/wow/dist/wow.js"></script>
        <script src="../lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="../lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="../lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="../lib/flexslider/jquery.flexslider.js"></script>
        <script src="../lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="../lib/smoothscroll.js"></script>
        <script src="../lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="../lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="../js/import/plugins.js"></script>
        <script src="../js/personal.js"></script>
    
    </body>
</html>