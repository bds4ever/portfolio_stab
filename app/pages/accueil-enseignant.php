<?php
session_start();

try
{
$bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

//requete pour afficher la liste des etudiants

// ici une requete qui ressemble a celle la mais avce laquelle on recuere aussi la mention
$reqrecherche = "SELECT * FROM utilisateurs WHERE role = 'etu'";
$req_prep = $bdd->prepare($reqrecherche.' ORDER BY id DESC ');
$req_prep->execute();



/****************************************** requete a faire ***********************/

// $users = $req_prep->fetchAll(); //pour avoir tout les utilisateurs

// foreach ($users as $user) { // cette boucle for tu dois la faire dans le cv et afficher ce que tu veux
//     $id_user = $user['id'];
//     $nom = $user['nom'];
//     $prenom = $user['prenom'];

//     $req = "SELECT DISTINCT m.* FROM mentions m
//                 INNER JOIN parcours_portfolio pf ON pf.id_mention = m.id
//                 WHERE pf.id_user = ?
//                 ORDER BY m.titre ASC";
//     $req = $bdd->prepare($req);
//     $req->execute(array($id_user));
//     $mentions = $req->fetchAll(); //toutes les mentions de l'utilisateur $user

    
/****************************************** fin requete a faire ***********************/

if (isset($_GET['id']) AND $_GET['id'] > 0)
{
    $getid = intval($_GET['id']);
    $requser = $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
    $requser->execute(array($getid));
    $userinfo = $requser->fetch();


     if (isset($_SESSION['id']) AND $userinfo['id'] == $_SESSION['id'])
    {


?>

<!-- cette page sera utilisée pour le personnel -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="../lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../lib/animate.css/animate.css" rel="stylesheet">
        <link href="../lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="../lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="../lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="../css/import/style.css" rel="stylesheet">
        <link href="../css/personal.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <main>
            <?php include('../includes/modal/confirmation-deconnexion.php'); ?>
            <?php include('../includes/modal/avis-sur-etudiant.php'); ?>
            <!-- include developpement de fonctionnalité à venir-->
            <?php include('../includes/modal/dev-to-come-up.php'); ?>
            <!-- include developpement de fonctionnalité à venir-->
            <?php include('../includes/modal/dev-to-come-up.php'); ?>

            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="../img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="#totop">Accueil</a></li>

                            <li>
                                <?php if ($userinfo['privilege'] == "admin") { ?>
                                <a href="management.php">Accès page de gestion</a>
                                <?php } ?>
                            </li>

                        </ul>

                         <!-- déconnexion -->
 
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a><?php  echo $userinfo['prenom']." ".$userinfo['nom']; ?></a>
                            </li>   
                                                     
                            <li>
                                <a href="deconnexion.php" role="button" aria-pressed="true">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>
                        </ul>                  
                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;../img/break.jpg&quot;);">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">
                                        Consulter les différents profils
                                    </div>

                                    <div class="font-alt mb-40 titan-title-size-4">SUIVI</div>
                                    <a class="section-scroll btn btn-border-w btn-round" href="#ancre_resultat_recherche_profil">
                                        Consulter
                                    </a>
                                </div>
                            </div>                        
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <!-- contenu -->
                <section class="module-medium" style="background: #fafafa;" id="ancre_resultat_recherche_profil">
                    <div class="container">
                        <div class="row">
                            <h2 class="module-subtitle  font-alt">Liste des profils</h2>
                            <!-- <form> -->
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="row mb-50">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group text-center">
                                                <input type="text" name="filtre" class="form-control dark" placeholder="filter suivant les champs" id="filter-data-accueil-enseignant" value="">
                                                        
                                            </div>
                                        </div>
                                    </div>



                            <!-- ********** ici ****************************** -->

                                    <div class="table-responsive-md">
                                        <table class="table table-hover bg-white table-striped">
                                            <thead>
                                                <tr title="cliquez pour être redirigé sur le portfolio de l'étudiant">
                                                    <th scope="col">Nom</th>
                                                    <th scope="col">Prénom</th>
                                                    <th scope="col">Formation</th>
                                                    <th scope="col">Niveau</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>

                                            <tbody id="body-accueil-enseignant">
                                                <?php while ($etudinfo = $req_prep->fetch()){ ?>
                                                <tr title="cliquez pour être redirigé sur le portfolio de l'étudiant">
                                                    <td class="text-left"><?php echo $etudinfo['nom'] ?></td>
                                                    <td><?php echo $etudinfo['prenom'] ?></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-left"><i class="fa fa-download"></i> Télécharger le cv de <?php echo $etudinfo['prenom']; ?></td>
                                                </tr>
                                                <?php } ?>

                            
                                            </tbody>
                                        </table>
                                    </div>

<!-- *************************************** -->

                                    <div class="form-group mb-2em hide">
                                        <label>Appréciation</label>
                                        <textarea rows="8" class="form-control" placeholder="..."></textarea>
                                    </div>

                                    <!-- boutons -->
                                    <div class="text-center hide">
                                        <button type="button" class="btn back form-group"
                                                name="" title="Déconnection suivie du retour à l'écran d'accueil"
                                                data-toggle="modal" data-target="#confirmation-deconnexion">

                                            <i class="fas fa-chevron-left"></i> Retour
                                        </button>

                                        <button type="submit" class="btn form-group" name="">
                                            Poster mon avis
                                        </button>
                                    </div>
                                    
                                </div>
                            <!-- </form> -->
                        </div>
                    </div>
                </section>

                <!-- script à déplacer asap -->
                <!-- script à déplacer asap -->
                <script type="text/javascript">
                    $("#filter-data-accueil-enseignant").keyup(function () {
                        //split the current value of searchInput
                        var data = this.value.split(" ");
                        //create a jquery object of the rows
                        var jo = $("#body-accueil-enseignant").find("tr");
                        if (this.value == "") {
                            jo.show();
                            return;
                        }
                        //hide all the rows
                        jo.hide();

                        //Recusively filter the jquery object to get results.
                        jo.filter(function (i, v) {
                            var $t = $(this);
                            for (var d = 0; d < data.length; ++d) {
                                if ($t.is(":contains('" + data[d] + "')")) {
                                    return true;
                                }
                            }
                            return false;
                        })
                        //show the rows that match.
                        .show();
                    }).focus(function () {
                        this.value = "";
                        $(this).css({
                            "color": "black"
                        });
                        $(this).unbind('focus');
                    }).css({
                        "color": "#C0C0C0"
                    });
                </script>
                <!-- script à déplacer asap -->
                <!-- script à déplacer asap -->

                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up" style="top: 45%"><a href="#totop"><i class="fa fa-angle-double-up fa-2x"></i></a></div>
        </main>
        
        <script src="../js/import/main.js"></script>
        <script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../lib/wow/dist/wow.js"></script>
        <script src="../lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="../lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="../lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="../lib/flexslider/jquery.flexslider.js"></script>
        <script src="../lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="../lib/smoothscroll.js"></script>
        <script src="../lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="../lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="../js/import/plugins.js"></script>
        <script src="../js/personal.js"></script>
    
    </body>
</html>
<?php
    /*on ferme lacolade du if $userinfo id == session id */
    }

/*on ferme lacolade du if isset get['id']...*/
}
?>

