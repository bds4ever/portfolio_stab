<?php
session_start();

try
{
$bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

/******************** ajout du cv ************************/

if (isset($_POST['ajout_img_utilisateur']))
            {
                
                $image_utilisateur = $_FILES['photo_utilisateur']['name']; //on recupere le nom du fichier à uploder
                $extension_image_utilisateur = strrchr($image_utilisateur, "."); //on recupere l'extension du fichier à uploder
                $taille_image_utilisateur = $_FILES['photo_utilisateur']['size']; //on recupere le nom du fichier à uploder
                $image_utilisateur_rep_tmp = $_FILES['photo_utilisateur']['tmp_name']; //on recupere l'adresse temporaire du fichier'
                $destination_fichier = '../img/photo_utilisateurs/'.$image_utilisateur;
                
                $extensions_autorisees = array('.png', '.PNG', '.jpg', '.JPG', '.jepg', '.JEPG');

                //si l'extension du fichier uploadé est dans le tableau des extensions autorisées alors on continue
                if (in_array($extension_image_utilisateur, $extensions_autorisees))
                {
                    if ($taille_image_utilisateur < 2000000)
                    {
                        if (!empty($image_utilisateur))
                        {
                            if (move_uploaded_file($image_utilisateur_rep_tmp, $destination_fichier)) //si le deplacement du fichier dans le repertoir img du site a reussi
                            {
                                $req_image_utilisateur = $bdd->prepare("INSERT INTO photo_utilisateurs SET user_id = ?, url_photo = ?");
                                $req_image_utilisateur->execute(array($_SESSION['id'], $destination_fichier));

                                $erreur_ajout = "Operation réussie avec succès";
                            }else{
                                $erreur_deplacement_fichier = "Une erreur est survenue lors de l'upload de l'image. Veuillez réesayer plus tard.";
                            }
                        }
                        else
                        {
                            $erreur_ajout = "Tous les champs doivent être complétés !";
                        }
                    }//fin if $taill...
                    else
                    {
                        $erreur_extension = "Taille maximale autorisé 2 Mo. Veuillez choisir un fichier moins volumineux";
                    }

                }
                else
                {
                    $erreur_extension = "Le type de fichier n'est pas valide, veuillez charger une iamge png ou jpg";
                }
            }

if (isset($_POST['ajout_cv_utilisateur']))
            {
                
                $cv_utilisateur = $_FILES['cv_utilisateur']['name']; //on recupere le nom du fichier à uploder
                $extension_cv_utilisateur = strrchr($cv_utilisateur, "."); //on recupere l'extension du fichier à uploder
                $taille_cv_utilisateur = $_FILES['cv_utilisateur']['size']; //on recupere le nom du fichier à uploder
                $cv_utilisateur_rep_tmp = $_FILES['cv_utilisateur']['tmp_name']; //on recupere l'adresse temporaire du fichier'
                $destination_cv = '../img/cv_utilisateurs/'.$cv_utilisateur;
                
                $extensions_autorisees = array('.pdf', '.PDF', '.doc', '.DOC', '.DOCX', '.DOCX');

                //si l'extension du fichier uploadé est dans le tableau des extensions autorisées alors on continue
                if (in_array($extension_cv_utilisateur, $extensions_autorisees))
                {
                    if ($taille_cv_utilisateur < 2000000)
                    {
                        if (!empty($cv_utilisateur))
                        {
                            if (move_uploaded_file($cv_utilisateur_rep_tmp, $destination_cv)) //si le deplacement du fichier dans le repertoir img du site a reussi
                            {
                                $req_image_utilisateur = $bdd->prepare("INSERT INTO cv_utilisateurs SET user_id = ?, url_cv = ?");
                                $req_image_utilisateur->execute(array($_SESSION['id'], $destination_cv));

                                $erreur_ajout = "Operation réussie avec succès";
                            }else{
                                $erreur_deplacement_fichier = "Une erreur est survenue lors de l'upload de l'image. Veuillez réesayer plus tard.";
                            }
                        }
                        else
                        {
                            $erreur_ajout = "Tous les champs doivent être complétés !";
                        }
                    }//fin if $taill...
                    else
                    {
                        $erreur_extension = "Taille maximale autorisé 2 Mo. Veuillez choisir un fichier moins volumineux";
                    }

                }
                else
                {
                    $erreur_extension = "Le type de fichier n'est pas valide, veuillez charger une iamge png ou jpg";
                }
            }
        


/********************* fin ajout cv **********************/

if (isset($_GET['id']) AND $_GET['id'] > 0)
{
    $getid = intval($_GET['id']);
    $requser = $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
    $requser->execute(array($getid));
    $userinfo = $requser->fetch();

     if (isset($_SESSION['id']))
    {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="../lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../lib/animate.css/animate.css" rel="stylesheet">
        <link href="../lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="../lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="../lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="../css/import/style.css" rel="stylesheet">
        <link href="../css/personal.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <?php include('../includes/modal/dev-to-come-up.php'); ?>
        <main>
            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="../img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="#totop">Accueil</a></li>

                            <!-- création de dossier perso -->
                            <li>
                                <a class="section-scroll" href="building.php?id=<?php echo $_SESSION['id'] ?>">Créer son dossier</a>
                            </li> 
                            <!-- consultation de dossier perso -->
                            <li>
                                <a class="section-scroll" href="portfolio-consultation.php?id=<?php echo $_SESSION['id'] ?>">
                                    Consulter son dossier
                                </a>
                            </li> 

                            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Raccourcis</a>
                            <ul class="dropdown-menu">
                                
                                <li><a class="section-scroll" href="#ancre_parcours">Parcours académique</a></li>
                                <li><a class="section-scroll" href="#ancre_diplome">Diplômes</a></li>
                                <li><a class="section-scroll" href="#ancre_competence">Compétences</a></li>

                                <li><a class="section-scroll" href="">Expériences</a></li>
                                <li><a class="section-scroll" href="#ancre_langue">Langues</a></li>
                                <li><a class="section-scroll" href="#ancre_centre_interet">Centres d'intérêt</a></li>
                                <li><a class="section-scroll" href="#ancre_contact_etudiant">Coordonnées</a></li>
                            </ul>
                        </ul>


                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a><?php  echo $userinfo['prenom']." ".$userinfo['nom']; ?></a>
                            </li>   
                                                     
                            <li>

                                <a href="deconnexion.php" role="button" aria-pressed="true">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>

                        </ul>                  

                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;../img/recherche.png&quot;);">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">Créer son dossier personnel</div>

                                    <div class="font-alt mb-40 titan-title-size-4">Création</div>

                                    <a class="section-scroll btn btn-border-w btn-round" href="#ancre_creation_portfolio">
                                        Démarrer
                                    </a>
                                </div>
                            </div>                        
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <!--<form>-->
                    <!-- pied de page du site -->
                    <section class="module-medium" id="ancre_creation_portfolio">
                        <!-- include developpement de fonctionnalité à venir-->
                        <?php include('../includes/modal/dev-to-come-up.php'); ?>
                        <!-- continuer process de creation -->
                        <?php include('../includes/modal/continuer-process.php'); ?>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 text-center">

                                <form action="" method="POST" role="form" enctype="multipart/form-data">
                                         
                                    <label class="btn free-access form-group" role="button">
                                        Importer votre CV <input type="file" name="cv_utilisateur" id="cv_utilisateur" hidden>
                                    </label>

                                    <button type="submit" class="btn access form-group" role="button"
                                                name="ajout_cv_utilisateur" id="ajout_fichier_utilisateur">
                                        sauvegarder le cv
                                    </button>
                                </form>

                                <form action="" method="POST" role="form" enctype="multipart/form-data">
                                    <label class="btn free-access form-group" role="button">
                                        Importer votre photo <input type="file"  name="photo_utilisateur" id="photo_utilisateur" hidden>
                                    </label><br><br>

                                    <button type="submit" class="btn access form-group" role="button"
                                                name="ajout_img_utilisateur" id="ajout_fichier_utilisateur">
                                        sauvegarder la photo
                                    </button>
                                </form>
                                </div>
                                <div class="text-center">
                                         <!-- Rappel : déplacer ces messages d'erreurs quelque part de mieux -->
                                        <?php
                                            if (isset($erreur_ajout)) {
                                                echo $erreur_ajout;
                                            }
                                            
                                            if (isset($erreur_extension)) {
                                                echo $erreur_extension;
                                            }

                                            if (isset($erreur_deplacement_fichier)) {
                                                echo $erreur_deplacement_fichier;
                                            }

                                                               
                                        ?>
                                    </div>
                            </div>
                        </div>
                    </section>   
                    <!-- background: #fafafa;  -->
                        
                    <section class="module-extra-small" style="background: linear-gradient(to bottom, white, #fafafa);">    
                        <div class="container">       
                            
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="row mb-50">          
                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s" >
                                            <div class="text-center">
                                                <a href="#ancre_projet_professionnel" class="section-scroll">
                                                <img src="../img/creation/projet.jpeg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                                Projet professionnel
                                            </div>
                                        </div>

                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.2s">
                                            <div class="text-center">
                                                <a href="#ancre_parcours" class="section-scroll">
                                                <img src="../img/creation/parcours.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                                 Parcours académique
                                            </div>
                                        </div>

                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                            <div class="text-center">
                                                <a href="#ancre_diplome" class="section-scroll">
                                                <img src="../img/creation/diplome.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                                Diplômes obtenus
                                            </div>
                                        </div>

                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                            <div class="text-center">
                                                <a href="#ancre_competence" class="section-scroll">
                                                <img src="../img/creation/competence.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                                Compétences
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">          
                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                            <div class="text-center">
                                                <a href="#ancre_experience_pro" class="section-scroll">
                                                <img src="../img/equipe_projet/bds3.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                             <div class="text-center mt-10">
                                                 Expériences professionnelles
                                             </div>
                                        </div>

                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.2s">
                                            <div class="text-center">
                                                <a href="#ancre_contact_etudiant" class="section-scroll">
                                                <img src="../img/creation/contact.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                                Contact
                                            </div>
                                        </div>

                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                            <div class="text-center">
                                                <a href="#ancre_langue" class="section-scroll">
                                                <img src="../img/creation/langue.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                                Connaissances linguistiques
                                            </div>
                                        </div>

                                        <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                            <div class="text-center">
                                                <a href="#ancre_experience_extra_pro" class="section-scroll">
                                                <img src="../img/equipe_projet/bds3.jpg"
                                                    style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                            </div>
                                            
                                            <div class="text-center mt-10">
                                               Expériences extra professionnelles
                                            </div>
                                        </div>
                                    </div> 
                                </div>                             
                            </div>       
                        </div>
                    </section>
 
                    <section class="module-medium" style="background: #fafafa;" id="ancre_projet_professionnel">
                        <?php include('../includes/view/building_element/projet_pro.php'); ?> 
                    </section>

                    <section class="module-medium" style="background: #fafafa;" id="ancre_parcours">
                        <?php include('../includes/view/building_element/parcours.php'); ?> 
                    </section>

                    <section class="module-medium" style="background: #fafafa;" id="ancre_diplome">
                        <?php include('../includes/view/building_element/diplome.php'); ?>
                    </section>       
                    
                    <section class="module-medium" style="background: #fafafa;" id="ancre_competence">
                        <?php include('../includes/view/building_element/competence.php'); ?>
                    </section>

                    <section class="module-medium" style="background: #fafafa;" id="ancre_experience_pro">
                        <?php include('../includes/view/building_element/experience_pro.php'); ?>
                    </section>

                    <section class="module-medium" style="background: #fafafa;" id="ancre_experience_extra_pro">
                        <?php include('../includes/view/building_element/experience_extra_pro.php'); ?>
                    </section>

                    <section class="module-medium" style="background: #fafafa;" id="ancre_langue">
                        <?php include('../includes/view/building_element/langue.php'); ?>
                    </section>
                    <!-- centre d'intérêt -->
                    <section class="module-medium" style="background: #fafafa;" id="ancre_centre_interet">
                        <?php include('../includes/view/building_element/centre_interet.php'); ?>
                    </section>
                    <!-- mes coordonnées -->
                    <section class="module-medium" style="background: #fafafa;" id="ancre_contact_etudiant">
                        <?php include('../includes/view/building_element/contact_etudiant.php'); ?> 
                    </section>

                <!--</form>-->

                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up" style="top: 45%"><a href="#totop"><i class="fa fa-angle-double-up fa-2x"></i></a></div>
        </main>
        
        <!-- import des différentes librairies javascript -->
        <!-- <script src="./lib/jquery/dist/jquery.js"></script> -->

        <!-- <script src="./js/jquery-3.3.1.js"></script> -->
        
        <script src="../js/import/main.js"></script>
        <script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../lib/wow/dist/wow.js"></script>
        <script src="../lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="../lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="../lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="../lib/flexslider/jquery.flexslider.js"></script>
        <script src="../lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="../lib/smoothscroll.js"></script>
        <script src="../lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="../lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="../js/import/plugins.js"></script>
        <script src="../js/personal.js"></script>
    
    </body>
</html>

<?php
    /*on ferme lacolade du if $userinfo id == session id */
    }
/*on ferme lacolade du if isset get['id']...*/
}
?>