<?php
session_start();

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');

    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}


                
                $req_projet_pro = $bdd->prepare("SELECT * FROM portfolio WHERE id_user = ?");
                $req_projet_pro->execute(array($_SESSION['id']));

                $req_projet_pro2 = $bdd->prepare("SELECT * FROM portfolio WHERE id_user = ?");
                $req_projet_pro2->execute(array($_SESSION['id']));

                $req_projet_pro3 = $bdd->prepare("SELECT * FROM portfolio WHERE id_user = ?");
                $req_projet_pro3->execute(array($_SESSION['id']));

                $req_projet_pro4 = $bdd->prepare("SELECT * FROM portfolio WHERE id_user = ?");
                $req_projet_pro4->execute(array($_SESSION['id']));

                $reqdiplome = $bdd->prepare("SELECT * FROM diplome_portfolio WHERE id_user = ?");
                $reqdiplome->execute(array($_SESSION['id']));

                $req_parcours = $bdd->prepare("SELECT * FROM parcours_portfolio WHERE id_user = ?");
                $req_parcours->execute(array($_SESSION['id']));

                $req_mention = $bdd->prepare("SELECT m.* FROM mentions m INNER JOIN parcours_portfolio pf ON pf.id_mention = m.id WHERE pf.id_user = ?");
                 $req_mention->execute(array($_SESSION['id']));

                 $req_niv_etu = $bdd->prepare("SELECT n.* FROM niveau_etude n INNER JOIN parcours_portfolio pf ON pf.id_niveau_etude = n.id WHERE pf.id_user = ?");
                 $req_niv_etu->execute(array($_SESSION['id']));

                 $req_domaine = $bdd->prepare("SELECT d.* FROM domaines d INNER JOIN parcours_portfolio pf ON pf.id_domaine = d.id WHERE pf.id_user = ?");
                 $req_domaine->execute(array($_SESSION['id']));



if (isset($_GET['id']) AND $_GET['id'] > 0)
{
    $getid = intval($_GET['id']);
    $requser = $bdd->prepare('SELECT * FROM utilisateurs WHERE id = ?');
    $requser->execute(array($getid));
    $userinfo = $requser->fetch();

     if (isset($_SESSION['id']) AND $userinfo->id == $_SESSION['id'])
    {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="../lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../lib/animate.css/animate.css" rel="stylesheet">
        <link href="../lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="../lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="../lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="../css/import/style.css" rel="stylesheet">
        <link href="../css/personal.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <?php include('../includes/modal/dev-to-come-up.php'); ?>
        <main>
            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="../img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="./homepage-student.php">Retour</a></li>
                            <!-- raccourci -->
                            <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Raccourcis</a>
                            <ul class="dropdown-menu">
                                <li><a class="section-scroll" href="#ancre-projet-consultation">Moi...</a></li>
                                <li><a class="section-scroll" href="#ancre-parcours-consultation">Parcours académique</a></li>
                                <li><a class="section-scroll" href="#ancre_diplome_consultation">Diplômes</a></li>
                                <li><a class="section-scroll" href="">Compétences</a></li>

                                <li><a class="section-scroll" href="">Expériences</a></li>
                                <li><a class="section-scroll" href="#ancre_langues_consultation">Langues</a></li>
                                <li><a class="section-scroll" href="#ancre_centre_interet_consultation">Centres d'intérêt</a></li>
                                <li><a class="section-scroll" href="#ancre_coordonnees_consultation">Coordonnées</a></li>
                            </ul>

                            <li><a class="section-scroll" href="#ancre_me_contacter">Me contacter</a></li>
                        </ul>


                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a><?php  echo $userinfo->prenom." ".$userinfo->nom; ?></a>
                            </li>   
                                                     
                            <li>

                                <a href="deconnexion.php" role="button" aria-pressed="true">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>
                        </ul>                 
                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url(&quot;../img/recherche.png&quot;);">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">Consultation de portfolio</div>

                                    <div class="font-alt mb-40 titan-title-size-4">Consultation</div>

                                    <a class="section-scroll btn btn-border-w btn-round" href="#ancre_creation_portfolio">
                                        Consulter
                                    </a>
                                </div>
                            </div>                        
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <section class="module-medium" id="ancre_projet_consulatation">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <h2 class="module-subtitle font-alt" id="ancre-projet-consultation">Bonjour!</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="text-center">
                                    <a href="http://google.com">
                                    <img src="../img/equipe_projet/bds3.jpg"
                                        style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                </div>

                                <?php while ($projet_pro = $req_projet_pro->fetch()){ ?>
                                            
                                    <h4><?php echo $projet_pro->titre; ?></h4>
                                    <div class="mt-50 border-consultation">
                                        <p>
                                            <?php echo $projet_pro->metier_aspire; ?>
                                        </p>

                                        <p class="mt-10">
                                            Pour en savoir davantage sur mon parcours ainsi que mes différentes expériences, descendez un peu plus bas.
                                        </p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- exo -->

<!--   <div class="text-center content-box-image">
                        <a href="#ancre_connexion_etudiant" onclick="afficherFormulaireEtudiant()" class="section-scroll">
                        <img src="./img/student-card.jpg "
                            style=" border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""
                            id="etudiant-card"></a>
                    </div> -->
                    
                <!-- exo -->

                <section class="module-medium" style="background: #fafafa;">    
                    <div class="container">       
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="row mb-50">          
                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s" >
                                        <div class="text-center">
                                            <a href="#ancre_projet_consulatation" class="section-scroll">
                                            <img src="../img/creation/projet.jpeg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                            Projet professionnel
                                        </div>
                                    </div>

                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.2s">
                                        <div class="text-center">
                                            <a href="#ancre_parcours_consultation" class="section-scroll">
                                            <img src="../img/creation/parcours.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                             Parcours académique
                                        </div>
                                    </div>

                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                        <div class="text-center">
                                            <a href="#ancre_diplome_consultation" class="section-scroll">
                                            <img src="../img/creation/diplome.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                            Diplômes obtenus
                                        </div>
                                    </div>

                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                        <div class="text-center">
                                            <a href="#ancre_competence_consultation" class="section-scroll">
                                            <img src="../img/creation/competence.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                            Compétences
                                        </div>
                                    </div>
                                </div>

                                <div class="row">          
                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                        <div class="text-center">
                                            <a href="http://google.com" class="section-scroll">
                                            <img src="../img/equipe_projet/bds3.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                         <div class="text-center mt-10">
                                             Expériences professionnelles
                                         </div>
                                    </div>

                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.2s">
                                        <div class="text-center">
                                            <a href="#ancre_me_contacter" class="section-scroll">
                                            <img src="../img/creation/contact.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                            Contact
                                        </div>
                                    </div>

                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                        <div class="text-center">
                                            <a href="#ancre_langues_consultation" class="section-scroll">
                                            <img src="../img/creation/langue.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                            Connaissances linguistiques
                                        </div>
                                    </div>

                                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.6s">
                                        <div class="text-center">
                                            <a href="http://google.com" class="section-scroll">
                                            <img src="../img/equipe_projet/bds3.jpg"
                                                style="width: 150px; height: 150px; border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                                        </div>
                                        
                                        <div class="text-center mt-10">
                                           Expériences extra professionnelles
                                        </div>
                                    </div>
                                </div> 
                            </div>                             
                        </div>       
                    </div>
                </section>
                
                <section class="module-medium" id="ancre_parcours_consultation">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <h2 class="module-subtitle font-alt">Parcours</h2>
                                <!-- alimenter -->


                                <!-- ************************************* -->

                                <table class="table bg-white table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Domaine</th>
                                                <th scope="col">Niveau d'études</th>
                                                <th scope="col">Mention</th>
                                                <th scope="col">Date de début</th>
                                                <th scope="col">Date de fin</th>
                                                <th scope="col">Motivations</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="liste_p">
                                           <?php while ( ($ment = $req_mention->fetch()) && ($parcours = $req_parcours->fetch()) && ($niveau_etu = $req_niv_etu->fetch()) && ($domain = $req_domaine->fetch()) ){ ?>  
                                            <tr>
                                                <td><?php echo $domain->titre; ?></td>
                                                <td><?php echo $niveau_etu->titre; ?></td>
                                                <td><?php echo $ment->titre; ?></td>
                                                <td><?php echo $parcours->date_debut; ?></td>
                                                <td><?php echo $parcours->date_fin; ?></td>
                                                <td><?php echo $parcours->motivations; ?></td>                                                
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>



                                <!-- ******************************** -->
                               
                            </div>
                        </div>
                    </div>
                </section>

                <section class="module-medium" id="ancre_diplome_consultation">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <h2 class="module-subtitle font-alt">Diplômes obtenus</h2>
                                

                                <!-- ************************************* -->

                                <table class="table bg-white table-striped">
                                        <thead>
                                            <tr">
                                                <th scope="col">Intitulé du diplômes</th>
                                                <th scope="col">Université / Institut / Ecole d'obtention du diplôme</th>
                                                <th scope="col">Année d'obtention</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="liste_p">
                                            <?php while ($diplome = $reqdiplome->fetch()){ ?>
                                            <tr>
                                                <td><?php echo $diplome->intitule; ?></td>
                                                <td><?php echo $diplome->ecole; ?></td>
                                                <td><?php echo $diplome->date_obtention; ?></td>                                                
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>

                                <!-- ************************************ -->
                            </div>
                        </div>
                    </div>
                </section>

                <section class="module-medium" id="ancre_langues_consultation">
                    <div class="container">
                        <div class="row">
                            <h2 class="module-subtitle  font-alt">Connaissances linguistiques</h2>
                            <form>
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label>Langues parlées</label>

                                         <?php while ($projet_pro2 = $req_projet_pro2->fetch()){ ?>
                                        <p><?php echo $projet_pro2->langues; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>

                <section class="module-medium" id="ancre_centre_interet_consultation">
                    <div class="container">
                        <div class="row">
                            <h2 class="module-subtitle  font-alt">Centres d'intérêt</h2>
                            <form>
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="form-group">
                                        <label>Activités passionnantes</label>
                                        <?php while ($projet_pro3 = $req_projet_pro3->fetch()){ ?>
                                        <p><?php echo $projet_pro3->interets; ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>


                <section class="module-medium" id="ancre_coordonnees_consultation">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">

                                <h2 class="module-subtitle font-alt">Coordonnées</h2>
                                <?php while ($projet_pro4 = $req_projet_pro4->fetch()){ ?>
                                <div class="form-group">
                                    <label>Téléphone</label>
                                    <label class="form-control"><?php echo $projet_pro4->phone; ?></label>
                                </div>

                                <div class="form-group">
                                    <label>Mail</label>
                                    <label class="form-control"><?php echo $projet_pro4->mail; ?></label>
                                </div>

                                <div class="form-group">
                                    <label>Réseau professionnel</label>
                                    <label class="form-control"><?php echo $projet_pro4->reseau_pro; ?></label>
                                </div>

                                <div class="form-group">
                                    <label>Région de résidence</label>
                                    <label class="form-control"><?php echo $projet_pro4->region; ?></label>
                                </div>

                                <div class="form-group">
                                    <label>Ville de résidence</label>
                                    <label class="form-control"><?php echo $projet_pro4->ville; ?></label>
                                </div>

                                <div class="form-group">
                                    <label>Mobilité géographique</label>
                                    <label class="form-control">
                                        <?php 
                                        if ($projet_pro4->mobilite == "true") {
                                            echo "Je suis disposé à travailler loin de chez moi";
                                        }else if($projet_pro4->mobilite == "false"){
                                            echo "Je ne suis pas disposé à travailler loin de chez moi";
                                        }
                                     ?>     
                                    </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="module-medium" id="ancre_me_contacter">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <h2 class="module-subtitle font-alt">Me contacter !</h2>

                                <form>
                                    <!-- contenu de m'ecrire -->
                                    <div class="form-group">
                                        <label>Nom</label>
                                        <input type="text" class="form-control" name="nom">
                                    </div>

                                    <div class="form-group">
                                        <label>Téléphone</label>
                                        <input type="tel" class="form-control" name="telephone">
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>

                                    <div class="form-group mb-2em">
                                        <label>Votre message</label>
                                        <textarea rows="4" class="form-control" name="comment" form="usrform"></textarea>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" value="Envoyer" class="btn validation min" onclick="envoyerMessage()">
                                            Envoyer
                                        </button>

                                        <button type="button" class="btn back min" onclick="retourSurPage('portfolioConsultation')"
                                            title="Retourner sur votre écran d'accueil">
                                            <i class="fas fa-chevron-left"></i> Retour
                                        </button>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up" style="top: 45%"><a href="#totop"><i class="fa fa-angle-double-up fa-2x"></i></a></div>
        </main>
        
        <!-- import des différentes librairies javascript -->
        <!-- <script src="./lib/jquery/dist/jquery.js"></script> -->

        <!-- <script src="./js/jquery-3.3.1.js"></script> -->
        
        <script src="../js/import/main.js"></script>
        <script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../lib/wow/dist/wow.js"></script>
        <script src="../lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="../lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="../lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="../lib/flexslider/jquery.flexslider.js"></script>
        <script src="../lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="../lib/smoothscroll.js"></script>
        <script src="../lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="../lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="../js/import/plugins.js"></script>
        <script src="../js/personal.js"></script>
    
    </body>
</html>

<?php
    /*on ferme lacolade du if $userinfo id == session id */
    }
/*on ferme lacolade du if isset get['id']...*/
}
?>