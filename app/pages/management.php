<?php
session_start();


     if (isset($_SESSION['id']))
    {
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="../lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../lib/animate.css/animate.css" rel="stylesheet">
        <link href="../lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="../lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="../lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="../css/import/style.css" rel="stylesheet">
        <link href="../css/personal.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <main>
            <?php include('../includes/modal/confirmation-deconnexion.php'); ?>
            <?php include('../includes/modal/avis-sur-etudiant.php'); ?>
            <!-- include developpement de fonctionnalité à venir-->
            <?php include('../includes/modal/dev-to-come-up.php'); ?>
            <!-- include developpement de fonctionnalité à venir-->
            <?php include('../includes/modal/dev-to-come-up.php'); ?>

            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="../img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="#totop">Accueil</a></li>
                        </ul>

                         <!-- déconnexion -->
 
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a><?php  echo $_SESSION['prenom']." ".$_SESSION['nom']; ?></a>
                            </li>   
                                                     
                            <li>
                                <a href="deconnexion.php" role="button" aria-pressed="true">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>
                        </ul>                  
                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url('../img/admin/admin.jpg');">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">
                                        Vous êtes dans l'espace de gestion !
                                    </div>

                                    <div class="font-alt mb-40 titan-title-size-4">PARAMETRAGE</div>
                                    <a class="section-scroll btn btn-border-w btn-round" href="#menu_param">
                                        Voir plus
                                    </a>
                                </div>
                            </div>                        
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <!-- contenu -->
                <section class="module-extra-small" style="background: #fafafa;" id="menu_param">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <h2 class="module-subtitle font-alt titan-title-size-2">Paramétrage</h2>

                                <div class="text-center">
                                    <a onclick="gererUtilisateur()" class="section-scroll btn free-access form-group parametrer-utilisateur"     href="#gerer-utilisateur" role="button">
                                            Personnel
                                    </a>

                                    <a onclick="gererMention()" class="section-scroll btn free-access form-group parametrer-mention" 
                                        href="#gerer-mention" role="button">
                                            Mentions
                                    </a>

                                    <a onclick="gererDomaineEtudes()" class="section-scroll btn free-access form-group  parametrer-domaine-etudes" href="#gerer-domaine-etudes" role="button">
                                            Formation
                                    </a>
                                </div>
                            </div>   
                        </div>
                    </div>
                </section>
                <!-- Gestion des domaines d'études -->
                <!-- Gestion des domaines d'études -->
                <section class="module-extra-small hide" id="gerer-domaine-etudes" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="text-center">
                                    <a onclick="insererDomaineEtudes()" class="section-scroll btn free-access form-group inserer-domaine-etudes"     href="#inserer-domaine-etudes" role="button">
                                            Insérer un domaine
                                    </a>

                                    <a onclick="listerDomaineEtudes()" class="section-scroll btn free-access form-group lister-domaine-etudes"     href="#liste-domaine-etudes" role="button">
                                            Liste des domaines
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>       

                <!-- Gestion des mentions -->
                <!-- Gestion des mentions -->
                <section class="module-extra-small hide" id="gerer-mention" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="text-center">
                                    <a onclick="insererMention()" class="section-scroll btn free-access form-group inserer-mention"  
                                            href="#inserer-mention" role="button">
                                            Nouvelle mention
                                    </a>

                                    <a onclick="listerMention()" class="section-scroll btn free-access form-group lister-mention"  
                                            href="#liste-mention" role="button">
                                            Liste des mentions
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Gestion des users -->
                <!-- Gestion des users -->
                <section class="module-extra-small hide" id="gerer-utilisateur" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="text-center">
                                    <a onclick="listerUtilisateur()" 
                                        class="section-scroll btn free-access form-group lister-utilisateur"    
                                         href="accorder-droit.php" role="button">
                                        Liste des enseignants
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- ajout d'un nouveau domaine -->
                <!-- ajout d'un nouveau domaine -->

                <section class="module-extra-small hide" id="inserer-domaine-etudes" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label>Libellé du domaine</label>
                                    <input  type="text" class="form-control" name="">
                                </div>
                                
                                <div class="mt-20 text-center">
                                    <button type="button" class="btn free-access" onclick=""
                                        aria-label="Enregistrer" >
                                        Enregistrer
                                    </button>

                                    <button type="button" class="btn cancel" onclick="fermerInsertion()"
                                        aria-label="Fermer" >
                                        Fermer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- ajout d'un nouvel utilisateur -->
                <!-- ajout d'un nouvel utilisateur -->

                <section class="module-extra-small hide ow fadeInUp" id="inserer-mention" 
                            style="background: #fafafa;" data-wow-delay="0.6s">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label>Code mention</label>
                                    <input  type="text" class="form-control" name="">
                                </div>

                                <div class="form-group">
                                    <label>Libellé de la mention</label>
                                    <input  type="text" class="form-control" name="">
                                </div>

                                <div class="form-group">
                                    <label>Domaine</label>
                                    <select class="form-control">
                                        <option selected>Sélectionnez</option>
                                        <option value="">Arts, Lettres, Langues</option>
                                        <option value="">Sciences humaines et sociales</option>
                                        <option value="">Droit, Economie, Gestion</option>
                                        <option value="">Sciences, Technologie, Santé</option>

                                        <option value="">
                                            Métiers de l’enseignement de l’éducation et de la formation
                                        </option>
                                    </select>
                                </div>
                                
                                <div class="mt-20 text-center">
                                    <button type="button" class="btn free-access" onclick=""
                                        aria-label="Enregistrer" >
                                        Enregistrer
                                    </button>

                                    <button type="button" class="btn cancel" onclick="fermerInsertion()"
                                        aria-label="Fermer" >
                                        Fermer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- ajout d'un  niveau d'étude -->
                <!-- ajout d'un  niveau d'étude -->
                
                <section class="module-extra-small hide" id="inserer-niveau-etudes" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label>Code niveau</label>
                                    <input  type="text" class="form-control" name="">
                                </div>

                                <div class="form-group">
                                    <label>Libellé du niveau</label>
                                    <input  type="text" class="form-control" name="">
                                </div>
                                        
                                <div class="mt-20 text-center">
                                    <button type="button" class="btn free-access" onclick=""
                                        aria-label="Enregistrer" >
                                        Enregistrer
                                    </button>

                                    <button type="button" class="btn cance" onclick="fermerInsertion()"
                                        aria-label="Fermer" >
                                        Fermer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- liste des mentions -->
                <!-- liste des mentions -->
                <section class="module-extra-small hide" id="liste-mention" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="table-responsive-md">
                                    <table class="table table-hover bg-white table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col">Code</th>
                                                <th scope="col">Libellé</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr id="mention1">
                                                <th scope="row">1</th>
                                                <td>MIAGE</td>
                                                <td>Otto</td>
                                                <td>
                                                    <i class="fa fa-edit hover-me" title="Modifier cette mention."
                                                       onclick="modifierMention()">
                                                    </i>

                                                    <i class="fa fa-trash-alt hover-me" title="Supprimer cette mention."
                                                       onclick="supprimerMention()">
                                                    </i>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Jacob</td>
                                                <td>Thornton</td>
                                                <td>
                                                    <i class="fa fa-edit hover-me" title="Modifier cette mention."
                                                       onclick="modifierMention()">
                                                    </i>

                                                    <i class="fa fa-trash-alt hover-me" title="Supprimer cette mention."
                                                       onclick="supprimerMention()">
                                                    </i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- liste des domaines -->
                <!-- liste des domaines -->

                <section class="module-extra-small hide" id="inserer-niveau-etudes" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="form-group">
                                    <label>Code niveau</label>
                                    <input  type="text" class="form-control" name="">
                                </div>

                                <div class="form-group">
                                    <label>Libellé du niveau</label>
                                    <input  type="text" class="form-control" name="">
                                </div>
                                        
                                <div class="mt-20 text-center">
                                    <button type="button" class="btn free-access" onclick=""
                                        aria-label="Enregistrer" >
                                        Enregistrer
                                    </button>

                                    <button type="button" class="btn cancel" onclick="fermerInsertion()"
                                        aria-label="Fermer" >
                                        Fermer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- liste des mentions -->
                <!-- liste des mentions -->
                <section class="module-extra-small hide" id="liste-domaine-etudes" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="table-responsive-md">
                                    <table class="table table-hover bg-white table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col">Libellé</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Sciences, techno, santé</td>
                                                <td>
                                                    <i class="fa fa-edit hover-me" title="Modifier cette mention."
                                                       onclick="modifierDomaine()">
                                                    </i>

                                                    <i class="fa fa-trash-alt hover-me" title="Supprimer cette mention."
                                                       onclick="supprimerDomaine()">
                                                    </i>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Droit, éco, gestion</td>
                                                <td>
                                                    <i class="fa fa-edit hover-me" title="Modifier cette mention."
                                                       onclick="modifierDomaine()">
                                                    </i>

                                                    <i class="fa fa-trash-alt hover-me" title="Supprimer cette mention."
                                                       onclick="supprimerDomaine()">
                                                    </i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="mt-20 text-center">
                                    <button type="button" class="btn free-access" onclick=""
                                        aria-label="Enregistrer" >
                                        Enregistrer
                                    </button>

                                    <button type="button" class="btn cancel" onclick="fermerInsertionDomaine()"
                                        aria-label="Fermer" >
                                        Fermer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up" style="top: 45%"><a href="#totop"><i class="fa fa-angle-double-up fa-2x"></i></a></div>
        </main>
        
        <script src="../js/import/main.js"></script>
        <script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../lib/wow/dist/wow.js"></script>
        <script src="../lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="../lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="../lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="../lib/flexslider/jquery.flexslider.js"></script>
        <script src="../lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="../lib/smoothscroll.js"></script>
        <script src="../lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="../lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="../js/import/plugins.js"></script>
        <script src="../js/personal.js"></script>
    
    </body>
</html>
<?php
    /*on ferme lacolade du if $userinfo id == session id */
    }
?>



