<?php
session_start();


     if (isset($_SESSION['id']))
    {
?>

<!-- cette page sera utilisée pour le personnel administrateur (pour creer ou supprimer dautre administarteur) -->

<?php 

/*connexion a la base de donnée*/
try
{
$bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
//pour afficher les erreurs liées à pdo
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

      
    
    //$reqcompte = "SELECT COUNT(*) AS total FROM utilisateurs WHERE role = 'ens'";




  //*************** pagination *******************

  // $ligneParPage=6; //nombres de lignes par page.

  // $retour_total= $bdd->prepare($reqcompte); // on prepare la requete : qui consite a compter le nombre de page
  // $retour_total->execute(); // on execute la requete
  // $donnees_total= $retour_total->fetch(); //on recupere la ligne qui contient le nombre total de page 

  
  // $nombreDePages=ceil($donnees_total->total/$ligneParPage); //on divise le nombre total de pages par le nombre de ligne par page et on arrondie au superieur grace a à la fonction ceil()

  // if(isset($_GET['page'])) // Si la variable $_GET['page'] existe (dans l'url), isset permet de tester l'existance d'une variable
  // {

  //   $pageActuelle=intval($_GET['page']); //transforme la valeur en parametre en valeur de type entier 

  //   if($pageActuelle>$nombreDePages) //si le numéro de page contenu dans $_GET['page'] est supérieur au nombre total de pages ($nombreDePage), 
  //   {
  //     $pageActuelle=$nombreDePages; //alors la page sera la page maximum
  //   }
  // }
  // else // Sinon si $_GET['page'] n'existe pas (si on a pas une url du style listemembre.php?page=[n° de la page]) la page actuelle est 1
  // {
  //   $pageActuelle=1; // La page actuelle est la n°1    
  // }

  // $premiereEntree=($pageActuelle-1)*$ligneParPage; // On calcul la première entrée à lire

  // // La requête sql pour récupérer les lignes de tableau de la page actuelle.
  // $retour_ligne = $bdd->prepare($reqrecherche.' ORDER BY id DESC LIMIT '.$premiereEntree.', '.$ligneParPage.'');
  // $retour_ligne->execute();
  
  //******************* fin pagination *********************



    // a supprimer si pagination dispo.





//requetes pour ajouter le privilege admin ou le retirer

    if (isset($_POST['submit_enseignant'])){
        $id_ens = $_POST['id_enseignant'];
        if (!empty($_POST['case_admin'])){

            $reqmaj = $bdd->prepare("UPDATE utilisateurs SET privilege = 'admin' WHERE id='".$id_ens."'");
            $reqmaj->execute();

            $info_admin = "Cet utilisateur est désormais administrateur";
            $signal = "vert";
        
                
        }else{

            $reqmaj = $bdd->prepare("UPDATE utilisateurs SET privilege = '' WHERE id='".$id_ens."'");
            $reqmaj->execute();

            $info_admin = "Cet utilisateur n'est plus administrateur";
            $signal = "rouge";

        }
    }

        $reqrecherche = "SELECT * FROM utilisateurs WHERE role = 'ens' AND id <> ".$_SESSION['id']."";
  $retour_ligne = $bdd->prepare($reqrecherche);
  $retour_ligne->execute();


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="author" content="Ben da SILVEIRA & Bilal AKAR">
        <title>Portfolio digital des étudiants de l'Université de Picardie Jules Verne d'Amiens</title>

        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link href="../lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../lib/animate.css/animate.css" rel="stylesheet">
        <link href="../lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="../lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="../lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="../lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="../lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <!-- import de couleur pour le template -->
        <link href="../css/import/style.css" rel="stylesheet">
        <link href="../css/personal.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <main>
            <?php include('../includes/modal/confirmation-deconnexion.php'); ?>
            <?php include('../includes/modal/forgotten-password_old.php'); ?>
            <!-- include developpement de fonctionnalité à venir-->
            <?php include('../includes/modal/dev-to-come-up.php'); ?>
            <!-- appliquer avant le chargement de toute la pge -->
            <div class="page-loader">
                <div class="loader">Chargement...</div>
            </div>
            <!-- positionnement de la navbar -->
            <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="#" class="navbar-brand" href="index.php">
                            <img src="../img/logo_blanc_30.png"
                            alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                        </a>
                    </div>

                    

                    <div class="collapse navbar-collapse" id="custom-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="" href="./management.php#totop">Retour</a></li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">                        
                            <li>
                                <a class="" href="#totop">
                                    <?php
                                        echo $_SESSION['prenom']." ".$_SESSION['nom'];
                                    ?>
                                </a>
                            </li>  

                            <li>
                                <a href="deconnexion.php" role="button" aria-pressed="true">
                                    <i class="fa fa-power-off fa-2x"></i>
                                </a>
                            </li>                      
                        </ul>                  
                    </div>
                </div>
            </nav>

            <!-- slider -->
            <section class="home-section home-full-height" id="home" >
                <div class="hero-slider" >
                    <ul class="slides" >
                        <li class="restaurant-page-header bg-dark" style="background-image:url('../img/admin/connexion_1.png');">
                            
                            <div class="titan-caption" >
                                <div class="caption-content" style="background: rgba(0, 0, 0, 0.6); display: table-cell;">
                                    <div class="font-alt mb-30 titan-title-size-1">
                                        Vous êtes dans l'espace de gestion !
                                    </div>

                                    <div class="font-alt mb-40 titan-title-size-4">Gestion des droits d'accès</div>
                                    <a class="section-scroll btn btn-border-w btn-round" href="#ancre_liste_enseignant">
                                        Voir plus
                                    </a>
                                </div>
                            </div>                        
                        </li>
                    </ul>
                </div>
            </section>
            
            <div class="main">
                <!-- liste des mentions -->
                <!-- liste des mentions -->
                <section class="module-extra-small" id="ancre_liste_enseignant" style="background: #fafafa;">
                    <div class="container">
                        <div class="row">          
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="table-responsive-md">
                                    <h2 class="module-subtitle font-alt titan-title-size-2">Liste des enseignants</h2>

                                    <div class="row mb-50">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group text-center">
                                                <input type="text" name="filtre" class="form-control dark" placeholder="filter suivant les champs" id="filtre_enseignant" value="">
                                                        
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table bg-white table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nom</th>
                                                <th scope="col">Prénom</th>
                                                <th scope="col">Administrateur</th>
                                                <th scope="col" class="">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody id="tableau_enseignant">
                                            <?php while ($enseignantinfo = $retour_ligne->fetch()){ ?>
                                            <tr>
                                                <form action="" method="POST" role="form">
                                                    <td> <?php echo $enseignantinfo->nom ?> </td>
                                                    <td> <?php echo $enseignantinfo->prenom ?> </td>
                                                    <td>
                                                        <!-- si privilege = admin dans la bdd alors la case est coché -->
                                                        <input type="checkbox" class="form-check-input" id="admin_check" name="case_admin"<?php if ($enseignantinfo->privilege == "admin" ){ echo " checked"; } ?>>
                                                        <input id="id_enseignant" name="id_enseignant" type="hidden" value="<?php echo $enseignantinfo->id ?>">
                                                        
                                                    </td>
                                                    <td >
                                                        <!-- <i class="fas fa-save"></i> 
                                                       <input type="submit" id="submit_enseignant" name="submit_enseignant" value="Enregistrer"> -->
                                                        <button type="submit" id="submit_enseignant" name="submit_enseignant"><i title="Cliquez pour enregistrer votre modification" class="fa fa-save fa-2x"></i> </button>

                                                    </td>
                                                </form>
                                                <!--  <td><i class="fas fa-download"></i> Télécharger le cv du profil</td> -->
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                            <!-- info donnant le statut de l'utilisateur,,, admin ou non -->
                            
                                <?php 
                                if (isset($info_admin) AND $signal == "vert") { ?>
                                    <div class="text-center msg_ajout_admin">
                                        <i class="fa fa-info-circle"></i>&nbsp;
                                        <?php echo $info_admin; ?>
                                    </div>
                                <?php
                                }else if(isset($info_admin) AND $signal == "rouge"){
                                     ?>
                                    <div class="text-center msg_supp_admin">
                                        <i class="fa fa-info-circle"></i>&nbsp;
                                        <?php echo $info_admin; ?>
                                    </div>
                                <?php

                                }
                                
                        //*************** navigation pour la pagination ********************
                        // echo '<p align="center">Page : '; //Pour l'affichage, on centre la liste des pages
                        // for($i=1; $i<=$nombreDePages; $i++) //On fait notre boucle
                        // {
                        // //On va faire notre condition
                        // if($i==$pageActuelle) //Si il s'agit de la page actuelle...
                        // {
                        //  echo ' [ '.$i.' ] '; 
                        // }  
                        // else //Sinon...
                        // {
                        //   echo ' <a href="tableau-recherche.php?page='.$i.'">'.$i.'</a> ';
                        // }
                        // }
                        // echo '</p>';
                        //**************** fin navigation pagination *********************************** ?>

                                    <!-- filtre du tableau -->

                                    <script type="text/javascript">
                                        $("#filtre_enseignant").keyup(function () {
                                            //split the current value of searchInput
                                            var data = this.value.split(" ");
                                            //create a jquery object of the rows
                                            var jo = $("#tableau_enseignant").find("tr");
                                            if (this.value == "") {
                                                jo.show();
                                                return;
                                            }
                                            //hide all the rows
                                            jo.hide();

                                            //Recusively filter the jquery object to get results.
                                            jo.filter(function (i, v) {
                                                var $t = $(this);
                                                for (var d = 0; d < data.length; ++d) {
                                                    if ($t.is(":contains('" + data[d] + "')")) {
                                                        return true;
                                                    }
                                                }
                                                return false;
                                            })
                                            //show the rows that match.
                                            .show();
                                        }).focus(function () {
                                            this.value = "";
                                            $(this).css({
                                                "color": "black"
                                            });
                                            $(this).unbind('focus');
                                        }).css({
                                            "color": "#C0C0C0"
                                        });
                                    </script>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>          

                <footer class="footer">
                    <div class="container">
                        <p class="text-white text-center">
                            Copyright <i class="fa fa-copyright"></i> 2018 - Tous droits réservés
                        </p>       
                    </div>
                </footer>
            </div>

            <div class="scroll-up" style="top: 45%"><a href="#totop"><i class="fa fa-angle-double-up fa-2x"></i></a></div>
        </main>
        
        <script src="../js/import/main.js"></script>
        <script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../lib/wow/dist/wow.js"></script>
        <script src="../lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="../lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="../lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="../lib/flexslider/jquery.flexslider.js"></script>
        <script src="../lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="../lib/smoothscroll.js"></script>
        <script src="../lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="../lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <!-- <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script> -->
        <script src="../js/import/plugins.js"></script>
        <script src="../js/personal.js"></script>
    
    </body>
</html>
<?php
    /*on ferme lacolade du if $userinfo id == session id */
    }
?>


