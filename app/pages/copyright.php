<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ePortfolio de l'UPJV</title>
        <!-- bootstrap css ok -->
        <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.css" />

        <!-- font-awesome css ok-->
        <link rel="stylesheet" href="../icons/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">
        <!-- index css ok-->
        <link rel="stylesheet" href="../css/home.css">
        <!-- favicon -->
        <link rel="icon" href="logo.png">

        <!-- jquery -->
        <script src="../js/jquery-3.3.1.js"></script>

        <script type="text/javascript" src="../js/personal.js"></script>
    </head>

    <body>
        <header class="container-fluid p-0 sticky-top">
            <nav class="navbar">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand" id="logo_gauche">
                        <img src="../img/logo_blanc_40.png"
                        alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                    </a>
                </div>

                <a  href="https://www.u-picardie.fr" target="_blank"
                    class="navbar-brand d-none d-sm-block">
                    <img src="../img/logoupjv-blanc_80.png" alt="logo du site"
                         title="visiter le site de l'université">
                </a>
            </nav>
        </header>

        <main class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                     <div class="text-center form-group">
                        <label class="font-weight-bold">
                            - Copyright -
                        </label>
                    </div>

                    <p class="text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt neque facere ducimus illum, adipisci, provident, nihil vitae iste aperiam quo quisquam harum, porro totam est reprehenderit. Doloremque neque itaque fuga. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis dolores expedita hic. Eaque illo placeat ea. Id architecto magnam eos hic accusantium. Reiciendis dolorem commodi molestiae nisi, blanditiis, distinctio modi.
                    </p>

                    <p class="text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus qui saepe quia animi amet ullam corporis cum culpa commodi velit dolores atque tempora, quae accusamus adipisci fugit illo magni unde.
                    </p>

                    <div class="text-center mt-4em">
                        <button type="button" class="btn back min form-group " onclick="retourSurPage('copyright')"
                            title="Retourner sur votre écran d'accueil">
                            <i class="fas fa-chevron-left"></i> Retour
                        </button>
                    </div>    
                </div>
            </div>
        </main>

        <footer class="container-fluid">
            <ul class="nav justify-content-center">
                <li class="nav-item" onclick="ouvrirContact('copyright')">
                    <a  class="nav-link text-white">
                        Contact
                    </a>
                </li>

                <li class="nav-item">
                    <a  class="nav-link text-white" onclick="ouvrirApropos('copyright')">
                        A propos
                    </a>
                </li>

                <li class="nav-item" title="Fonctionnalité à venir!" disabled>
                    <a class="nav-link text-white" >Langue</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" disabled>
                        Copyright <i class="far fa-copyright"></i> 2018
                    </a>
                </li>
            </ul>
        </footer>
        <!-- jquery, ajax, bootstrap -->
        <script src="../js/popper.js"></script>
        <script src="../js/bootstrap/js/bootstrap.js"></script>
    </body>
</html>


