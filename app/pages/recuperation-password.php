<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ePortfolio de l'UPJV</title>
        <!-- bootstrap css ok -->
        <link rel="stylesheet" href="../css/bootstrap/css/bootstrap.css" />

        <!-- font-awesome css ok-->
        <link rel="stylesheet" href="../icons/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">
        <!-- index css ok-->
        <link rel="stylesheet" href="../css/home.css">
        <!-- favicon -->
        <link rel="icon" href="logo.png">

        <!-- jquery -->
        <script src="../js/jquery-3.3.1.js"></script>

        <script type="text/javascript" src="../js/personal.js"></script>
    </head>

    <body>
        <header class="container-fluid p-0 sticky-top">
            <nav class="navbar">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand" id="logo_gauche">
                        <img src="../img/logo_blanc_40.png"
                        alt="logo du site" title="portfolio digital de l'Université de Picardie Jules Verne">
                    </a>
                </div>

                <a  href="https://www.u-picardie.fr" target="_blank"
                    class="navbar-brand d-none d-sm-block">
                    <img src="../img/logoupjv-blanc_80.png" alt="logo du site"
                         title="visiter le site de l'université">
                </a>
            </nav>
        </header>

        <main class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <div class="text-center form-group mb-2em">
                        <label class="font-weight-bold">
                            - Récupération de mot de passe -
                        </label>
                    </div>

                    <div class="form-group mb-4em">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
             

                    <div class="text-center form-group">
                        <button type="button" class="btn validation min form-group">
                            Valider
                        </button>

                        <button type="button" class="btn back min form-group " onclick="retourSurPage('recuperation-password')"
                            title="Retourner sur votre écran d'accueil">
                            <i class="fas fa-chevron-left"></i> Retour
                        </button>
                    </div> 
                </div>
            </div>
        </main>

        <footer class="container-fluid">
            <ul class="nav justify-content-center">
                <li class="nav-item" onclick="ouvrirContact('recuperation-password')">
                    <a  class="nav-link text-white">
                        Contact
                    </a>
                </li>

                <li class="nav-item">
                    <a  class="nav-link text-white" onclick="ouvrirApropos('recuperation-password')">
                        A propos
                    </a>
                </li>

                <li class="nav-item" title="Fonctionnalité à venir!" disabled>
                    <a class="nav-link text-white" >Langue</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" onclick="ouvrirCopyright('recuperation-password')">
                        Copyright <i class="far fa-copyright"></i> 2018
                    </a>
                </li>
            </ul>
        </footer>
        <!-- jquery, ajax, bootstrap -->
        <script src="../js/popper.js"></script>
        <script src="../js/bootstrap/js/bootstrap.js"></script>
    </body>
</html>


