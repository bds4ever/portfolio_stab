<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="nav-link section-scroll" href="#contacter-univ">Contact</a>
            </li>

            <li class="nav-item">
                <a  class="nav-link section-scroll" href="#digifolio"> A propos </a> 
            </li>

            <li class="nav-item">
                <a class="nav-link" onclick="ouvrirCopyright('index')">
                    Copyright <i class="fa fa-copyright"></i> 2018
                </a>
            </li>  
        </ul>
    </div>
</div>   