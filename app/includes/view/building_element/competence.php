<div class="container" id="projet-pro">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h2 class="module-subtitle font-alt">Compétences</h2>
        </div>
    </div>

    <form>
        <div class="row">
            <div class="col-md-10 col-sm-offset-1">
                <div class="table-responsive-md">
                    <table class="table table-hover bg-white table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Domaines</th>
                                <th scope="col">Compétences</th>
                                <th scope="col">e-1</th>
                                <th scope="col">e-2</th>
                                <th scope="col">e-3</th>
                                <th scope="col">e-4</th>
                                <th scope="col">e-5</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>Planifier</td>
                                <td>Gestion des niveaux de services</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>

                                <td>Planifier</td>
                                <td>Mise en place d’un plan d’activités</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>
                                <td>Faciliter</td>
                                <td>Développement d’une stratégie de sécurité de l’information</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>
                                
                                <td>Faciliter</td>
                                <td>Prestation de services de formation</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>  
                                <td>Développer</td>
                                <td>Conception et développement d’applications</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>  
                                <td>Développer</td>
                                <td>Intégration des systèmes</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>  
                                <td>Gérer</td>
                                <td>Gestion des projets et du portefeuille de projets</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>

                            <tr>  
                                <td>Gérer</td>
                                <td>Développement prévisionnel</td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                                <td><input type="checkbox" name="" class="form-control"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- boutons -->
        <div class="text-center mt-50 mb-50">
            <button type="submit" class="btn free-access form-group">
                Enregistrer
            </button>
        </div>
    </form>
</div>