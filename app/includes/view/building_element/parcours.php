<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
} catch (Exception $e) {
    echo "Erreur lors de la connexion à la base de donnée !";
    return;
}

//Requete HTTP POST (Ajax)
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['parcours'])){
        session_start();
        $id_user = intval($_SESSION['id']);
        $parcours = $_POST['parcours'];

        $json_response = array();

        foreach ($parcours as $p){
            $id = intval($p['id']);
            $domaine = intval($p['id_domaine']);
            $niveau_etude = intval($p['id_niveau_etude']);
            $mention = intval($p['id_mention']);

            $date_debut = (!empty(date($p['date_debut']))) ? date($p['date_debut']) : null ;
            $date_fin = (!empty(date($p['date_fin']))) ? date($p['date_fin']) : null;
            $motivations = trim(htmlspecialchars($p['motivations']));

            if($id == -1){
                try {
                    $stmt = $bdd->prepare("INSERT INTO parcours_portfolio (id_domaine, id_niveau_etude, id_mention, date_debut, date_fin, motivations, id_user) VALUES (:id_domaine, :id_niveau_etude, :id_mention, :date_debut, :date_fin, :motivations, :id_user)");
                    $stmt->bindParam(':id_domaine', $domaine);
                    $stmt->bindParam(':id_niveau_etude', $niveau_etude);
                    $stmt->bindParam(':id_mention', $mention);
                    $stmt->bindParam(':date_debut', $date_debut);
                    $stmt->bindParam(':date_fin', $date_fin);
                    $stmt->bindParam(':motivations', $motivations);
                    $stmt->bindParam(':id_user', $id_user);
                    $stmt->execute();

                    $json_response["new_id"] = array();
                    $last_id = $bdd->lastInsertId();
                    array_push($json_response["new_id"], $last_id);

                }catch (Exception $e){
                    echo $e->getTraceAsString();
                }
            }

            if($id > -1){
                $reqparcours = $bdd->prepare('SELECT * FROM parcours_portfolio WHERE id = ? AND id_user = ?');
                $reqparcours->execute(array($id, $id_user));
                $p = $reqparcours->fetch();

                if ($p) {
                    try {
                        $stmt = $bdd->prepare("UPDATE parcours_portfolio SET id_domaine=:id_domaine, id_niveau_etude=:id_niveau_etude, id_mention=:id_mention, date_debut=:date_debut, date_fin=:date_fin, motivations=:motivations WHERE id=:id AND id_user=:id_user");
                        $stmt->bindParam(':id_domaine', $domaine);
                        $stmt->bindParam(':id_niveau_etude', $niveau_etude);
                        $stmt->bindParam(':id_mention', $mention);
                        $stmt->bindParam(':date_debut', $date_debut);
                        $stmt->bindParam(':date_fin', $date_fin);
                        $stmt->bindParam(':motivations', $motivations);
                        $stmt->bindParam(':id', $id);
                        $stmt->bindParam(':id_user', $id_user);
                        $stmt->execute();
                    }catch (Exception $e){
                        echo $e->getTraceAsString();
                    }
                }
            }
        }

        $json_response["response"] = "Sauvegarde réussie !";
        echo json_encode($json_response);

    }else{
        $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
        echo json_encode($json_response);
    }
    return;
}


//Recuperation des données existantes
$id = intval($_SESSION['id']);
$reqparcours = $bdd->prepare('SELECT * FROM parcours_portfolio WHERE id_user = ? ORDER BY date_fin DESC');
$reqparcours->execute(array($id));
$parcours = $reqparcours->fetchAll();

?>

<div class="container" id="parcours">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-subtitle  font-alt">Parcours académique</h2>
        </div>
    </div>
    
    <div class="row">
        <form id="parcoursForm">
            <?php if(count($parcours)==0): ?>
                <div class="col-sm-6 col-sm-offset-3"  id="clonage-parcours">
                    <input type="text" id="-1" hidden>

                    <div class="form-group">
                        <label> Domaine </label>

                        <?php
                            $req_domaines = $bdd->prepare('SELECT * FROM domaines');
                            $req_domaines->execute();
                            $domaines = $req_domaines->fetchAll();
                        ?>

                        <select id="slct0" class="form-control" name="slct0" onchange="onDomaineSelect(event)">
                            <option value="selectionDomaine" selected disabled>Sélectionnez</option>
                            <?php foreach ($domaines as $domaine): ?>
                                <option value="<?= $domaine['id'] ?>"><?= $domaine['titre'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group" id="montrerNiveauEtudes">
                        <label>Niveau d'études</label>
                        <select class="form-control" id="slct1" name="slct1" onchange="onNiveauEtudesSelect(event)">
                            <option value="selectionNiveauEtudes" disabled selected>Sélectionnez</option>
                        </select>
                    </div>

                    <div class="form-group" id="montrerMention">
                        <label>Mention</label>
                        <select class="form-control" id="slct2" name="slct2">
                            <option value="selectionMention" disabled selected>Sélectionnez</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Date de début</label>
                        <input id="date_debut" type="date" name="dateDebutFormation" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Date de fin</label>
                        <input id="date_fin" type="date" name="dateFinFormation" class="form-control">
                    </div>

                    <div class="form-group" id="motivation">
                        <label>Motivations</label>
                        <textarea id="motivations" class="form-control comment-area" maxlength="300" rows="5" placeholder="Evoquez les principales raisons justifiant le choix de cette formation"></textarea>
                    </div>
                </div>
            <?php else: ?>
                <?php foreach ($parcours as $p): ?>
                    <div class="col-sm-6 col-sm-offset-3"  id="clonage-parcours">
                        <input type="text" id="<?= $p['id'] ?>" hidden>

                        <div class="form-group">
                            <label> Domaine </label>

                            <?php
                                $req_domaines = $bdd->prepare('SELECT * FROM domaines');
                                $req_domaines->execute();
                                $domaines = $req_domaines->fetchAll();
                            ?>

                            <select id="slct0" class="form-control" name="slct0" onchange="onDomaineSelect(event)">
                                <option value="selectionDomaine" selected disabled>Sélectionnez</option>
                                <?php foreach ($domaines as $domaine): ?>
                                    <option <?= ($domaine['id'] == $p['id_domaine'])? "selected": "" ?> value="<?= $domaine['id'] ?>"><?= $domaine['titre'] ?>  </option>
                                <?php endforeach; ?>
                            </select>
                        </div>


                        <?php
                            $req = "SELECT DISTINCT ne.* FROM niveau_etude ne 
                                    INNER JOIN domaine_niveau_mention dnm 
                                    ON dnm.id_niveau_etude = ne.id
                                    AND dnm.id_domaine = ?
                                    ORDER BY ne.titre ASC";

                            $req_niveau_etudes = $bdd->prepare($req);
                            $req_niveau_etudes->execute(array($p['id_domaine']));
                            $niveau_etudes = $req_niveau_etudes->fetchAll();
                        ?>

                        <div class="form-group" id="montrerNiveauEtudes">
                            <label>Niveau d'études</label>
                            <select class="form-control" id="slct1" name="slct1" onchange="onNiveauEtudesSelect(event)">
                                <option value="selectionNiveauEtudes" disabled selected>Sélectionnez</option>
                                <?php foreach ($niveau_etudes as $etude): ?>
                                    <option <?= ($etude['id'] == $p['id_niveau_etude'])? "selected": "" ?> value="<?= $etude['id'] ?>"><?= $etude['titre'] ?>  </option>
                                <?php endforeach; ?>
                            </select>
                        </div>


                        <?php
                            $req = "SELECT DISTINCT m.* FROM mentions m
                            INNER JOIN domaine_niveau_mention dnm ON dnm.id_mention = m.id
                            WHERE dnm.id_domaine = ? AND dnm.id_niveau_etude = ?
                            ORDER BY m.titre ASC";

                            $req_mentions = $bdd->prepare($req);
                            $req_mentions->execute(array($p['id_domaine'], $p['id_niveau_etude']));
                            $mentions = $req_mentions->fetchAll();
                        ?>


                        <div class="form-group" id="montrerMention">
                            <label>Mention</label>
                            <select class="form-control" id="slct2" name="slct2">
                                <option value="selectionMention" disabled selected>Sélectionnez</option>
                                <?php foreach ($mentions as $mention): ?>
                                    <option <?= ($mention['id'] == $p['id_mention'])? "selected": "" ?> value="<?= $mention['id'] ?>"><?= $mention['titre'] ?>  </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Date de début</label>
                            <input id="date_debut" type="date" name="dateDebutFormation" class="form-control" value="<?= ($p['date_debut'])? date($p['date_debut']):"" ?>">
                        </div>

                        <div class="form-group">
                            <label>Date de fin</label>
                            <input id="date_fin" type="date" name="dateFinFormation" class="form-control" value="<?= ($p['date_fin'])? date($p['date_fin']):"" ?>">
                        </div>

                        <div class="form-group" id="motivation">
                            <label>Motivations</label>
                            <textarea id="motivations" class="form-control comment-area" maxlength="300" rows="5" placeholder="Evoquez les principales raisons justifiant le choix de cette formation"><?=$p['motivations']?></textarea>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <!-- espace de clonage -->
            <div class="clone-parcours"></div>

            <div class="col-sm-6 col-sm-offset-3">
                <div class="text-center mt-50 mb-50">
                    <button type="button" class="btn free-access form-group" onclick="ajouterAutreParcours()">
                        <i class="fa fa-plus"></i> Parcours
                    </button>

                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>


<script>
    $("#parcoursForm").submit(function (event) {
        event.preventDefault();

        var parcours = [];

        $("#parcoursForm #clonage-parcours").each(function(index) {
            var p = $(this);

            var id = p.find('input').first().attr('id');
            var domaine = p.find("#slct0").val();
            var niveau_etude = p.find("#slct1").val();
            var mention = p.find("#slct2").val();
            var date_debut = p.find("#date_debut").val();
            var date_fin = p.find("#date_fin").val();
            var motivations = p.find("#motivations").val();

            parcours.push({
                id: id,
                id_domaine: domaine,
                id_niveau_etude: niveau_etude,
                id_mention: mention,
                date_debut: date_debut,
                date_fin: date_fin,
                motivations: motivations
            });
        });

        var data = {
            parcours: parcours
        };

        $.post(
            "../includes/view/building_element/parcours.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                var new_id = data_response['new_id'];

                var new_id_length = new_id.length;
                var parcours_id_update = $("#parcoursForm #clonage-parcours");
                var parcours_length = parcours_id_update.length;

                for(var i=new_id_length; i>0; i--){
                    parcours_id_update[parcours_length-i].getElementsByTagName('input')[0].id = new_id[new_id_length-i];
                }
            }
        });
    })
</script>
