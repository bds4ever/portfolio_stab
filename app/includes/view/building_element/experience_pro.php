<?php
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
    } catch (Exception $e) {
        echo "Erreur lors de la connexion à la base de donnée !";
        return;
    }

    //Requete HTTP POST (Ajax)
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['exps'])){

            session_start();
            $id_user = intval($_SESSION['id']);
            $exps = $_POST['exps'];

            $json_response = array();

            foreach ($exps as $exp){
                $id = intval($exp['id']);
                $titre = trim(htmlspecialchars($exp['titre']));
                $date_debut = (!empty(date($exp['date_debut']))) ? date($exp['date_debut']) : null ;
                $date_fin = (!empty(date($exp['date_fin']))) ? date($exp['date_fin']) : null;
                $raison_sociale = trim(htmlspecialchars($exp['raison_sociale']));
                $statut = trim(htmlspecialchars($exp['statut']));
                $description = trim(htmlspecialchars($exp['description']));

                if($id == -1){
                    try {
                        $stmt = $bdd->prepare("INSERT INTO experience_pro_portfolio (titre, date_debut, date_fin, raison_sociale, statut, description, id_user) VALUES (:titre, :date_debut, :date_fin, :raison_sociale, :statut, :description, :id_user)");
                        $stmt->bindParam(':titre', $titre);
                        $stmt->bindParam(':date_debut', $date_debut);
                        $stmt->bindParam(':date_fin', $date_fin);
                        $stmt->bindParam(':raison_sociale', $raison_sociale);
                        $stmt->bindParam(':statut', $statut);
                        $stmt->bindParam(':description', $description);
                        $stmt->bindParam(':id_user', $id_user);
                        $stmt->execute();

                        $json_response["new_id"] = array();
                        $last_id = $bdd->lastInsertId();
                        array_push($json_response["new_id"], $last_id);

                    }catch (Exception $e){
                        echo $e->getTraceAsString();
                    }
                }

                if($id > -1){
                    $reqexps = $bdd->prepare('SELECT * FROM experience_pro_portfolio WHERE id = ? AND id_user = ?');
                    $reqexps->execute(array($id, $id_user));
                    $exp = $reqexps->fetch();

                    if ($exp) {
                        try {
                            $stmt = $bdd->prepare("UPDATE experience_pro_portfolio SET titre=:titre, date_debut=:date_debut, date_fin=:date_fin, raison_sociale=:raison_sociale, statut=:statut, description=:description WHERE id=:id AND id_user=:id_user");
                            $stmt->bindParam(':titre', $titre);
                            $stmt->bindParam(':date_debut', $date_debut);
                            $stmt->bindParam(':date_fin', $date_fin);
                            $stmt->bindParam(':raison_sociale', $raison_sociale);
                            $stmt->bindParam(':statut', $statut);
                            $stmt->bindParam(':description', $description);
                            $stmt->bindParam(':id', $id);
                            $stmt->bindParam(':id_user', $id_user);
                            $stmt->execute();

                            $json_response["response"] = "Modification des données réussie !";

                        }catch (Exception $e){
                            echo $e->getTraceAsString();
                        }
                    }
                }
            }

            $json_response["response"] = "Sauvegarde réussie !";
            echo json_encode($json_response);

        }else{
            $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
            echo json_encode($json_response);
        }
        return;
    }


    //Recuperation des données existantes
    $id = intval($_SESSION['id']);
    $reqexp = $bdd->prepare('SELECT * FROM experience_pro_portfolio WHERE id_user = ? ORDER BY date_fin DESC');
    $reqexp->execute(array($id));
    $experiences = $reqexp->fetchAll();

?>

<div class="container" id="proExp">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-subtitle  font-alt">Expériences professionnelles</h2>
        </div>
    </div>

    <div class="row">
        <form id="experienceProForm">
            <?php if(count($experiences)==0): ?>
                <div class="col-sm-6 col-sm-offset-3"  id="clonage-experiences-professionnelles">
                    <input type="text" id="-1" hidden>

                    <div class="form-group">
                        <label>Titre</label>
                        <input id="titre" type="text" class="form-control" name="fonctionOccupee">
                    </div>

                    <div class="form-group">
                        <label>Date de début</label>
                        <input id="date_debut" type="date" name="dateDebutExperiencePro" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Date de fin</label>
                        <input id="date_fin" type="date" name="dateFinExperiencePro" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Raison sociale de l'entreprise</label>
                        <input id="raison_sociale" type="text" class="form-control" name="nomEntreprise" >
                    </div>

                    <div class="form-group">
                        <label>Sur cette période, vous étiez (êtes)</label>
                        <select id="statut" class="form-control">
                            <option selected disabled>Sélectionnez</option>
                            <option value="">Stagiaire</option>
                            <option value="">Apprenti</option>
                            <option value="">En CDI</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Description de la mission réalisée</label>
                        <textarea id="description" class="form-control comment-area" rows="6" maxlength="400"></textarea>
                    </div>
                </div>

            <?php else: ?>
                <?php foreach ($experiences as $experience): ?>
                    <div class="col-sm-6 col-sm-offset-3"  id="clonage-experiences-professionnelles">
                        <input type="text" id="<?= $experience['id'] ?>" hidden>

                        <div class="form-group">
                            <label>Titre</label>
                            <input id="titre" type="text" class="form-control" name="fonctionOccupee" value="<?=$experience['titre']?>">
                        </div>

                        <div class="form-group">
                            <label>Date de début</label>
                            <input id="date_debut" type="date" name="dateDebutExperiencePro" class="form-control" value="<?= ($experience['date_debut'])? date($experience['date_debut']):"" ?>">
                        </div>

                        <div class="form-group">
                            <label>Date de fin</label>
                            <input id="date_fin" type="date" name="dateFinExperiencePro" class="form-control" value="<?= ($experience['date_fin'])? date($experience['date_fin']):"" ?>">
                        </div>

                        <div class="form-group">
                            <label>Raison sociale de l'entreprise</label>
                            <input id="raison_sociale" type="text" class="form-control" name="nomEntreprise" value="<?= $experience['raison_sociale'] ?>" >
                        </div>

                        <div class="form-group">
                            <label>Sur cette période, vous étiez (êtes)</label>
                            <select id="statut" class="form-control">
                                <option selected disabled>Sélectionnez</option>
                                <option value="Stagiaire" <?= ($experience['statut'] == "Stagiaire")? "selected": "" ?> >Stagiaire</option>
                                <option value="Apprenti" <?= ($experience['statut'] == "Apprenti")? "selected": "" ?>>Apprenti</option>
                                <option value="CDI" <?= ($experience['statut'] == "CDI")? "selected": "" ?>>En CDI</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Description de la mission réalisée</label>
                            <textarea id="description" class="form-control comment-area" rows="6" maxlength="400"><?=$experience['description']?></textarea>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <!-- espace de clonage -->
            <div class="clone-experience-professionelle"></div>

            <!-- boutons -->
            <div class="col-sm-6 col-sm-offset-3">
                <div class="text-center  mt-50 mb-50">
                    <button type="button" class="btn form-group free-access" onclick="ajouterAutreExperiencePro()">
                        <i class="fa fa-plus"></i> Expérience
                    </button>

                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>

<script>
    $("#experienceProForm").submit(function (event) {
        event.preventDefault();

        var exps = [];

        $("#experienceProForm #clonage-experiences-professionnelles").each(function(index) {
            var exp = $(this);

            var id = exp.find('input').first().attr('id');
            var titre = exp.find("#titre").val();
            var date_debut = exp.find("#date_debut").val();
            var date_fin = exp.find("#date_fin").val();
            var raison_sociale = exp.find("#raison_sociale").val();
            var statut = exp.find("#statut").val();
            var description = exp.find("#description").val();

            exps.push({
                id: id,
                titre: titre,
                date_debut: date_debut,
                date_fin: date_fin,
                raison_sociale: raison_sociale,
                statut: statut,
                description: description
            });
        });

        var data = {
            exps: exps
        };

        $.post(
            "../includes/view/building_element/experience_pro.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                var new_id = data_response['new_id'];

                var new_id_length = new_id.length;
                var exps_id_update = $("#experienceProForm #clonage-experiences-professionnelles");
                var exps_length = exps_id_update.length;

                for(var i=new_id_length; i>0; i--){
                    exps_id_update[exps_length-i].getElementsByTagName('input')[0].id = new_id[new_id_length-i];
                }
            }
        });
    })
</script>