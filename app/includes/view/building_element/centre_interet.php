<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
} catch (Exception $e) {
    echo "Erreur lors de la connexion à la base de donnée !";
    return;
}

//Requete HTTP POST (Ajax)
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['interets'])) {
        session_start();
        $id = intval($_SESSION['id']);
        $interets = trim(htmlspecialchars($_POST['interets']));

        $json_response = array();

        $reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
        $reqportfolio->execute(array($id));
        $portfolio = $reqportfolio->fetch();

        if ($portfolio) {
            $stmt = $bdd->prepare("UPDATE portfolio SET interets=:interets WHERE id_user=:id_user");
            $stmt->bindParam(':id_user', $id);
            $stmt->bindParam(':interets', $interets);
            $stmt->execute();
        } else {
            $stmt = $bdd->prepare("INSERT INTO portfolio (id_user, interets) VALUES (:id_user, :interets)");
            $stmt->bindParam(':id_user', $id);
            $stmt->bindParam(':interets', $interets);
            $stmt->execute();
        }

        $json_response['response'] = "Sauvegarde reussie !";
        echo json_encode($json_response);

    } else {
        $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
        echo json_encode($json_response);
    }

    return;
}


//Recuperation des données existantes
$id = intval($_SESSION['id']);
$reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
$reqportfolio->execute(array($id));
$portfolio = $reqportfolio->fetch();

?>

<div class="container" id="centre_interet">
    <form id="interetsForm">
        <div class="row">
       
            <h2 class="module-subtitle  font-alt">Centres d'intérêt</h2>
        
            <div class="col-sm-6 col-sm-offset-3">
                <div class="form-group">
                    <label>Activités passionnantes</label>
                    <textarea id="interets" class="form-control" rows="6" maxlength="400"><?=$portfolio['interets']?></textarea>
                </div>
                <!-- bouton -->

                <div class="text-center mt-50 mb-50">
                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div>  
            </div>
        </div>
    </form>
</div>


<script>
    $("#interetsForm").submit(function (event) {
        event.preventDefault();

        var interets = $("#interetsForm #interets").val();
        console.log(interets);

        var data = {
            interets: interets
        };

        $.post(
            "../includes/view/building_element/centre_interet.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                console.log("yes");
            }
        });
    })
</script>