<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
} catch (Exception $e) {
    echo "Erreur lors de la connexion à la base de donnée !";
    return;
}

//Requete HTTP POST (Ajax)
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['diplomes'])){
        session_start();
        $id_user = intval($_SESSION['id']);
        $diplomes = $_POST['diplomes'];

        $json_response = array();

        foreach ($diplomes as $diplome){
            $id = intval($diplome['id']);
            $intitule = trim(htmlspecialchars($diplome['intitule']));
            $ecole = trim(htmlspecialchars($diplome['ecole']));
            $date_obtention = (!empty(date($diplome['date_obtention']))) ? date($diplome['date_obtention']) : null ;

            if($id == -1){
                try {
                    $stmt = $bdd->prepare("INSERT INTO diplome_portfolio (intitule, ecole, date_obtention, id_user) VALUES (:intitule, :ecole, :date_obtention, :id_user)");
                    $stmt->bindParam(':intitule', $intitule);
                    $stmt->bindParam(':ecole', $ecole);
                    $stmt->bindParam(':date_obtention', $date_obtention);
                    $stmt->bindParam(':id_user', $id_user);
                    $stmt->execute();

                    $json_response["new_id"] = array();
                    $last_id = $bdd->lastInsertId();
                    array_push($json_response["new_id"], $last_id);

                }catch (Exception $e){
                    echo $e->getTraceAsString();
                }
            }

            if($id > -1){
                $reqdiplome = $bdd->prepare('SELECT * FROM diplome_portfolio WHERE id = ? AND id_user = ?');
                $reqdiplome->execute(array($id, $id_user));
                $diplome = $reqdiplome->fetch();

                if ($diplome) {
                    try {
                        $stmt = $bdd->prepare("UPDATE diplome_portfolio SET intitule=:intitule, ecole=:ecole, date_obtention=:date_obtention WHERE id=:id AND id_user=:id_user");
                        $stmt->bindParam(':intitule', $intitule);
                        $stmt->bindParam(':ecole', $ecole);
                        $stmt->bindParam(':date_obtention', $date_obtention);
                        $stmt->bindParam(':id', $id);
                        $stmt->bindParam(':id_user', $id_user);
                        $stmt->execute();
                        echo "Modification ...";
                    }catch (Exception $e){
                        echo $e->getTraceAsString();
                    }
                }
            }
        }

        $json_response["response"] = "Sauvegarde réussie !";
        echo json_encode($json_response);

    }else{
        $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
        echo json_encode($json_response);
    }
    return;
}


//Recuperation des données existantes
$id = intval($_SESSION['id']);
$reqdiplome = $bdd->prepare('SELECT * FROM diplome_portfolio WHERE id_user = ? ORDER BY date_obtention DESC');
$reqdiplome->execute(array($id));
$diplomes = $reqdiplome->fetchAll();

?>

<div class="container" id="diplome">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <h2 class="module-subtitle  font-alt">Diplômes obtenus</h2>
        </div>
    </div>

    <div class="row">
        <form id="diplomeForm">
            <?php if(count($diplomes)==0): ?>
                <div class="col-sm-6 col-sm-offset-3"  id="clonage-diplome">
                    <input type="text" id="-1" hidden>

                    <div class="form-group">
                        <label>
                            Intitulé du diplôme
                            <i class="fa fa-info-circle" title="Renseignez les diplômes obtenus après le baccalauréat."></i>
                        </label>

                        <input id="intitule" type="text" class="form-control" name="" placeholder="Licence">
                    </div>

                    <div class="form-group">
                        <label>Université / Institut / Ecole d'obtention du diplôme</label>
                        <input id="ecole" type="text" class="form-control" name="" placeholder="Université de Picardie Jules Verne">
                    </div>

                    <div class="form-group">
                        <label>Année d'obtention</label>
                        <input id="date_obtention" type="date" class="form-control">
                    </div>
                </div>
            <?php else: ?>
                <?php foreach ($diplomes as $diplome): ?>
                    <div class="col-sm-6 col-sm-offset-3"  id="clonage-diplome">
                        <input type="text" id="<?= $diplome['id'] ?>" hidden>

                        <div class="form-group">
                            <label>
                                Intitulé du diplôme
                                <i class="fa fa-info-circle" title="Renseignez les diplômes obtenus après le baccalauréat."></i>
                            </label>

                            <input id="intitule" type="text" class="form-control" name="" placeholder="Licence" value="<?= $diplome['intitule'] ?>">
                        </div>

                        <div class="form-group">
                            <label>Université / Institut / Ecole d'obtention du diplôme</label>
                            <input id="ecole" type="text" class="form-control" name="" placeholder="Université de Picardie Jules Verne" value="<?= $diplome['ecole'] ?>">
                        </div>

                        <div class="form-group">
                            <label>Année d'obtention</label>
                            <input id="date_obtention" type="date" class="form-control" value="<?= ($diplome['date_obtention'])? date($diplome['date_obtention']):"" ?>">
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif; ?>

            <!-- zone de clonage -->
            <div class="clone-diplome"></div>

            <div class="col-sm-6 col-sm-offset-3">
                <div class="text-center mt-50 mb-50">
                    <button type="button" class="btn free-access form-group" onclick="ajouterAutreDiplome()">
                        <i class="fa fa-plus"></i> Diplôme
                    </button>

                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $("#diplomeForm").submit(function (event) {
        event.preventDefault();

        var diplomes = [];

        $("#diplomeForm #clonage-diplome").each(function(index) {
            var diplome = $(this);

            var id = diplome.find('input').first().attr('id');
            var intitule = diplome.find("#intitule").val();
            var ecole = diplome.find("#ecole").val();
            var date_obtention = diplome.find("#date_obtention").val();

            diplomes.push({
                id: id,
                intitule: intitule,
                ecole: ecole,
                date_obtention: date_obtention
            });
        });

        var data = {
            diplomes: diplomes
        };

        $.post(
            "../includes/view/building_element/diplome.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                var new_id = data_response['new_id'];

                var new_id_length = new_id.length;
                var diplome_id_update = $("#diplomeForm #clonage-diplome");
                var diplome_length = diplome_id_update.length;

                console.log(new_id_length);
                console.log(diplome_id_update);
                console.log(diplome_length);

                for(var i=new_id_length; i>0; i--){
                    diplome_id_update[diplome_length-i].getElementsByTagName('input')[0].id = new_id[new_id_length-i];
                }
            }
        });
    })
</script>