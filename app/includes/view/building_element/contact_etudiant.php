<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
} catch (Exception $e) {
    echo "Erreur lors de la connexion à la base de donnée !";
    return;
}

//Requete HTTP POST (Ajax)
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['tel']) && isset($_POST['email']) && isset($_POST['reseau_pro']) && isset($_POST['region']) && isset($_POST['ville']) && isset($_POST['mobilite'])) {
        session_start();
        $id = intval($_SESSION['id']);
        $tel = trim(htmlspecialchars($_POST['tel']));
        $email = trim(htmlspecialchars($_POST['email']));
        $reseau_pro = trim(htmlspecialchars($_POST['reseau_pro']));
        $region = trim(htmlspecialchars($_POST['region']));
        $ville = trim(htmlspecialchars($_POST['ville']));
        $mobilite = trim(htmlspecialchars($_POST['mobilite']));

        $json_response = array();

        $reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
        $reqportfolio->execute(array($id));
        $portfolio = $reqportfolio->fetch();

        if ($portfolio) {
            $stmt = $bdd->prepare("UPDATE portfolio SET phone=:phone, mail=:mail, reseau_pro=:reseau_pro, region=:region, ville=:ville, mobilite=:mobilite WHERE id_user=:id_user");
            $stmt->bindParam(':id_user', $id);
            $stmt->bindParam(':phone', $tel);
            $stmt->bindParam(':mail', $email);
            $stmt->bindParam(':reseau_pro', $reseau_pro);
            $stmt->bindParam(':region', $region);
            $stmt->bindParam(':ville', $ville);
            $stmt->bindParam(':mobilite', $mobilite);
            $stmt->execute();
        } else {
            $stmt = $bdd->prepare("INSERT INTO portfolio (id_user, phone, mail, reseau_pro, region, ville, mobilite) VALUES (:id_user, :phone, :mail, :reseau_pro, :region, :ville, :mobilite)");
            $stmt->bindParam(':id_user', $id);
            $stmt->bindParam(':phone', $tel);
            $stmt->bindParam(':mail', $email);
            $stmt->bindParam(':reseau_pro', $reseau_pro);
            $stmt->bindParam(':region', $region);
            $stmt->bindParam(':ville', $ville);
            $stmt->bindParam(':mobilite', $mobilite);
            $stmt->execute();


        }

        $json_response['response'] = "Sauvegarde reussie !";
        echo json_encode($json_response);

    } else {
        $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
        echo json_encode($json_response);
    }

    return;
}


//Recuperation des données existantes
$id = intval($_SESSION['id']);
$reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
$reqportfolio->execute(array($id));
$portfolio = $reqportfolio->fetch();

?>

<div class="container" id="">
    <form id="contactEtudiantForm">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-subtitle font-alt">Me contacter</h2>

                <div class="form-group">
                    <label>Téléphone</label>
                    <input id="tel" type="tel" class="form-control" name="" placeholder="0123456789" value="<?=$portfolio['phone']?>">
                </div>

                <div class="form-group">
                    <label>Mail</label>
                    <input id="email" type="email" class="form-control" name="" value="<?=$portfolio['mail']?>">
                </div>

                <div class="form-group">
                    <label>Réseau professionnel</label>
                    <input id="reseau_pro" type="text" class="form-control" name="" value="<?=$portfolio['reseau_pro']?>">
                </div>

                <div class="form-group">
                    <label>Région de résidence</label>
                    <input id="region" type="text" class="form-control" name="" value="<?=$portfolio['region']?>">
                </div>

                <div class="form-group">
                    <label>Ville de résidence</label>
                    <input id="ville" type="text" class="form-control" name="" value="<?=$portfolio['ville']?>">
                </div>

                <div class="form-group">
                    <label>Mobilité géographique</label>

                    <select id="mobilite" class="form-control">
                        <option selected disabled>Sélectionnez</option>
                        <option value="true" <?= ($portfolio['mobilite'] == "true")? "selected": "" ?>>Je suis disposé à travailler loin de chez moi</option>
                        <option value="false" <?= ($portfolio['mobilite'] == "false")? "selected": "" ?>>Je ne suis pas disposé à travailler loin de chez moi</option>
                    </select>
                </div>

                <div class="text-center mt-50 mb-50">
                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div> 
            </div>
        </div>
    </form>
</div>

<script>
    $("#contactEtudiantForm").submit(function (event) {
        event.preventDefault();

        var tel = $("#contactEtudiantForm #tel").val();
        var email = $("#contactEtudiantForm #email").val();
        var reseau_pro = $("#contactEtudiantForm #reseau_pro").val();
        var region = $("#contactEtudiantForm #region").val();
        var ville = $("#contactEtudiantForm #ville").val();
        var mobilite = $("#contactEtudiantForm #mobilite").val();

        var data = {
            tel: tel,
            email: email,
            reseau_pro: reseau_pro,
            region: region,
            ville: ville,
            mobilite: mobilite
        };

        $.post(
            "../includes/view/building_element/contact_etudiant.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                console.log("yes");
            }
        });
    })
</script>