<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
} catch (Exception $e) {
    echo "Erreur lors de la connexion à la base de donnée !";
    return;
}

//Requete HTTP POST (Ajax)
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['langues'])) {
        session_start();
        $id = intval($_SESSION['id']);
        $langues = trim(htmlspecialchars($_POST['langues']));

        $json_response = array();

        $reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
        $reqportfolio->execute(array($id));
        $portfolio = $reqportfolio->fetch();

        if ($portfolio) {
            $stmt = $bdd->prepare("UPDATE portfolio SET langues=:langues WHERE id_user=:id_user");
            $stmt->bindParam(':id_user', $id);
            $stmt->bindParam(':langues', $langues);
            $stmt->execute();
        } else {
            $stmt = $bdd->prepare("INSERT INTO portfolio (id_user, langues) VALUES (:id_user, :langues)");
            $stmt->bindParam(':id_user', $id);
            $stmt->bindParam(':langues', $langues);
            $stmt->execute();
        }

        $json_response['response'] = "Sauvegarde reussie !";
        echo json_encode($json_response);

    } else {
        $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
        echo json_encode($json_response);
    }

    return;
}


//Recuperation des données existantes
$id = intval($_SESSION['id']);
$reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
$reqportfolio->execute(array($id));
$portfolio = $reqportfolio->fetch();

?>


<div class="container" id="langues">
    <form id="languesForm">
        <div class="row">
        
            <h2 class="module-subtitle  font-alt">Connaissances linguistiques</h2>
        
            <div class="col-sm-6 col-sm-offset-3">
                <div class="form-group">
                    <!-- <label>Langues parlées</label> -->
                    <textarea id="langues" class="form-control" rows="6" maxlength="400" placeholder="Je parle couramment..."><?=$portfolio['langues']?></textarea>
                </div>

                <div class="text-center mt-50 mb-50">
                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div>
            </div>             
        </div>
    </form>
</div>


<script>
    $("#languesForm").submit(function (event) {
        event.preventDefault();

        var langues = $("#languesForm #langues").val();
        console.log(langues);

        var data = {
            langues: langues
        };

        $.post(
            "../includes/view/building_element/langue.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                console.log("yes");
            }
        });
    })
</script>


