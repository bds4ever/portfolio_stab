<?php
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
    } catch (Exception $e) {
        echo "Erreur lors de la connexion à la base de donnée !";
        return;
    }

    //Requete HTTP POST (Ajax)
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['titre']) && isset($_POST['metier_aspire'])) {
            session_start();
            $id = intval($_SESSION['id']);
            $titre = trim(htmlspecialchars($_POST['titre']));
            $metier_aspire = trim(htmlspecialchars($_POST['metier_aspire']));

            $json_response = array();

            $reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
            $reqportfolio->execute(array($id));
            $portfolio = $reqportfolio->fetch();

            if ($portfolio) {
                $stmt = $bdd->prepare("UPDATE portfolio SET titre=:titre, metier_aspire=:metier_aspire WHERE id_user=:id_user");
                $stmt->bindParam(':id_user', $id);
                $stmt->bindParam(':titre', $titre);
                $stmt->bindParam(':metier_aspire', $metier_aspire);
                $stmt->execute();
            } else {
                $stmt = $bdd->prepare("INSERT INTO portfolio (id_user, titre, metier_aspire) VALUES (:id_user, :titre, :metier_aspire)");
                $stmt->bindParam(':id_user', $id);
                $stmt->bindParam(':titre', $titre);
                $stmt->bindParam(':metier_aspire', $metier_aspire);
                $stmt->execute();
            }

            $json_response['response'] = "Sauvegarde reussie !";
            echo json_encode($json_response);

        } else {
            $json_response["response"] = "Les données n'ont pas pu être sauvegardées !";
            echo json_encode($json_response);
        }

        return;
    }


    //Recuperation des données existantes
    $id = intval($_SESSION['id']);
    $reqportfolio = $bdd->prepare('SELECT * FROM portfolio WHERE id_user = ?');
    $reqportfolio->execute(array($id));
    $portfolio = $reqportfolio->fetch();

?>

<div class="container" id="projet-pro">
    <form id="projetForm" role="form">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-subtitle font-alt">Projet professionnel</h2>

                <div class="form-group">
                    <label for="titre">Titre</label>
                    <input id="titre" type="text" class="form-control" value="<?=($portfolio) ? $portfolio['titre'] : "" ?>">
                </div>

                <div class="form-group">
                    <label for="metierAspire">Parlez nous un peu plus de votre projet</label>
                    <textarea id="metierAspire" class="form-control comment-area" maxlength="500" rows="6" placeholder="Vos aspirations..."><?=($portfolio) ? $portfolio['metier_aspire'] : "" ?></textarea>
                </div>

                <!-- bouton -->
                <div class="text-center mt-50 mb-50">
                    <button type="submit" class="btn free-access form-group">
                        Enregistrer
                    </button>
                </div> 
            </div>
        </div>
    </form>
</div>

<script>
    $("#projetForm").submit(function (event) {
        event.preventDefault();

        var titre = $("#projetForm #titre").val();
        var metierAspire = $("#projetForm #metierAspire").val();

        var data = {
            titre: titre,
            metier_aspire: metierAspire
        };

        $.post(
            "../includes/view/building_element/projet_pro.php",
            data
        ).done(function (response) {
            var data_response = JSON.parse(response);
            if(data_response['new_id']){
                console.log("yes");
            }
        });
    })
</script>