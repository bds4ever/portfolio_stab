<div class="container">  
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h2 class="module-subtitle font-alt"> Retrouver un profil </h2> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">  
            <form method="post" action="">
                <div class="form-group" >
                    <input class="form-control form-group dark" type="text" placeholder="Nom" aria-label="Nom" name="recherchenom">
                </div>

                <div class="form-group">
                    <input class="form-control form-group dark" type="text" placeholder="Prénom" aria-label="Prénom" name="rechercheprenom">
                </div>

                <div class="text-center mt-50">
                    <button type="submit" class="btn free-access" aria-label="Chercher" name="lancerRecherche" value="Rechercher">
                        Retrouver
                    </button>
                </div>
            </form>       
        </div>         
    </div>
</div>
