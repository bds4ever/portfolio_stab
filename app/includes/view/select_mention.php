<?php

session_start();

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

if(isset($_POST['id_domaine']) && isset($_POST['id_niveau_etude'])){
    $id_domaine = intval($_POST['id_domaine']);
    $id_niveau = intval($_POST['id_niveau_etude']);

    $req = "SELECT DISTINCT m.* FROM mentions m
            INNER JOIN domaine_niveau_mention dnm ON dnm.id_mention = m.id
            WHERE dnm.id_domaine = ? AND dnm.id_niveau_etude = ?
            ORDER BY m.titre ASC";

    $req_mentions = $bdd->prepare($req);
    $req_mentions->execute(array($id_domaine, $id_niveau));
    $mentions = $req_mentions->fetchAll();

    echo json_encode($mentions);
    return;
}
?>