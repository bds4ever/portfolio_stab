<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h2 class="module-subtitle font-alt"> Connexion</h2>
        </div>
    </div>
    <!-- include -->
    <?php include('./includes/modal/forgotten-password_old.php'); ?>
    <?php include('./includes/modal/dev-to-come-up.php'); ?>
    <!-- blocs d'images pour ouvrir une connexion -->

    <div class="row multi-columns-row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row mb-50">          
                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="text-center content-box-image">
                        <a href="#ancre_connexion_etudiant" onclick="afficherFormulaireEtudiant()" class="section-scroll">
                        <img src="./img/student-card.jpg "
                            style=" border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""
                            id="etudiant-card"></a>
                    </div>
                    
                     <div class="text-center mt-10">
                        Etudiant
                     </div>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center content-box-image">
                        <a href="#ancre_connexion_enseignant" onclick="afficherFormulaireEnseignant()" class="section-scroll">
                        <img src="./img/personnal-card.png" id="enseignant-card"
                            style=" border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                    </div>
                    
                    <div class="text-center mt-10">
                        Enseignant
                    </div>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="text-center content-box-image">
                        <a href="#ancre_connexion_entreprise" onclick="afficherFormulaireEntreprise()" class="section-scroll">
                        <img src="./img/company-card.png" id="entreprise-card"
                            style=" border-radius: 2px; box-shadow: 1px 1px 12px #555;" alt=""></a>
                    </div>
                    
                    <div class="text-center mt-10">
                        Entreprise
                    </div>
                </div>
            </div>           
        </div>                             
    </div> 

    <!-- broui -->
    <section class="module hide" id="formulaire-etudiant">
        <div class="row" id="ancre_connexion_etudiant">
            <div class="col-md-4 col-md-offset-4">
                <div class="text-center">
                    <button type="button" class="btn btn-connexion-etudiant"
                        onclick="afficherConnexionEtudiant()" aria-label="Connexion">
                        Connexion
                    </button>

                    <button type="button" class="btn btn-inscription-etudiant"
                            aria-label="Inscription"
                            onclick="afficherInscriptionEtudiant()">
                        Inscription
                    </button>
                </div>           
            </div>         
        </div>
        
        <div class="row mt-50">
            <div class="col-md-4 col-md-offset-4 arounder ">
                <!-- connexion etudiant -->
                <div id="connexion-etudiant" class="">
                    <form action="" method="POST" role="form">
                        <div class="form-group">
                            <!-- Email -->
                            <label>Email</label>
                            <input  type="email" class="form-control dark" name="mailconnect">                     
                        </div>
                        <!-- Mot de passe -->
                        <div class="form-group">
                            <label>Mot de passe</label>
                            <input  type="password" class="form-control dark"  name="mdpconnect" id="pwd-etudiant-saisi">
                        </div>

                        <script type="text/javascript">
                             
                             function togglePassword(el){
                             
                              // Checked State
                              var checked = el.checked;

                              if(checked){
                               // Changing type attribute
                               document.getElementById("pwd-etudiant-saisi").type = 'text';
                               document.getElementById("pwd-enseignant-saisi").type = 'text';
                               document.getElementById("pwd-entreprise-saisi").type = 'text';

                              }else{
                               // Changing type attribute
                               document.getElementById("pwd-etudiant-saisi").type = 'password';
                               document.getElementById("pwd-enseignant-saisi").type = 'password';
                               document.getElementById("pwd-entreprise-saisi").type = 'password';

                              }

                             }
                             
                        </script>

                        <div class="form-check">
                            <input type='checkbox' >
                            <small class="form-check-label">Afficher le mot de passe</small>
                        </div>


                        <div class="text-right form-group mb-2em hover-me" onclick="ouvrirRecuperationPassword()">
                            <small>Mot de passe oublié ?</small>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn free-access form-group"
                                    name="formconnexion" >
                                Se connecter
                            </button>

                            <button type="submit" class="btn cancel form-group"
                                    name="">
                                Fermer
                            </button>
                        </div>
                    </form>
                </div>

                <!-- inscription etudiant -->
                <div id="inscription-etudiant" class="hide">
                    <form action="" method="POST" role="form">
                        <div>
                            <div class="form-group">
                                <label>Nom</label>
                                <input  type="text" class="form-control dark" name="nom">
                            </div>

                            <div class="form-group">
                                <label > Prénom </label>
                                <input  type="text" class="form-control dark" name="prenom" id="prenom">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input  type="email" class="form-control dark" name="mail">
                            </div>

                            <div class="form-group">
                                <label> Mot de passe </label>
                                <input  type="password" class="form-control dark" name="mdp">
                            </div>

                            <div class="form-group">
                                <label> Confirmation de mot de passe </label>
                                <input  type="password" class="form-control dark" name="mdp2">
                            </div>
                        </div>

                        <div class="text-center mt-40">
                            <button type="submit" class="btn free-access form-group"
                                    name="forminscription_etu">
                                Valider
                            </button>

                            <button type="submit" class="btn cancel min form-group"
                                    name="">
                                Fermer
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Rappel : déplacer ces messages d'erreurs quelque part de mieux -->
    <?php
        if (isset($erreur_inscrption)) {
            echo $erreur_inscrption;
        }

        if (isset($erreur_co)) {
            echo $erreur_co;
        }
    ?>
    <section class="module hide" id="formulaire-enseignant">
        <div class="row" id="ancre_connexion_enseignant">
            <div class="col-md-4 col-md-offset-4">
                <div class="text-center">
                    <button type="button" class="btn btn-connexion-enseignant"
                        onclick="afficherConnexionEnseignant()" aria-label="Connexion">
                        Connexion
                    </button>

                    <button type="button" class="btn btn-inscription-enseignant"
                            aria-label="Inscription" onclick="afficherInscriptionEnseignant()">                 
                        Inscription
                    </button>
                </div>           
            </div>         
        </div>
        
        <div class="row mt-50">
            <div class="col-md-4 col-md-offset-4 arounder ">
                <!-- connexion enseignant -->
                <div id="connexion-enseignant" class="">
                    <form action="" method="POST" role="form">
                                                
                        <div class="form-group">
                            <!-- Email -->
                            <label>Email</label>
                            <input  type="email" class="form-control dark" name="mailconnect"> 
                        </div>

                        <!-- Mot de passe -->
                        <div class="form-group">
                            <label>Mot de passe</label>
                            <input  type="password" class="form-control dark" name="mdpconnect">        
                        </div>

                        <div class="form-check">
                            <input type='checkbox' >
                            <small class="form-check-label">Afficher le mot de passe</small>
                        </div>

                        <div class="text-right form-group hover-me" onclick="ouvrirRecuperationPassword()">
                            <small>Mot de passe oublié ?</small>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn free-access form-group" name="formconnexion" >
                                Se connecter
                            </button>

                            <button type="submit" class="btn cancel form-group">
                                Fermer
                            </button>
                        </div>               
                    </form>
                </div>
                <!-- inscription enseignant -->
                <!-- inscription enseignant -->
                <div class="hide" id="inscription-enseignant">
                    <form action="" method="POST" role="form"  >
                        <div class="mb-4em">
                            <div class="form-group">
                                <label>Nom</label>
                                <input  type="text" class="form-control dark" name="nom">
                            </div>

                            <div class="form-group">
                                <label>Prénom</label>
                                <input  type="text" class="form-control dark" name="prenom">
                            </div>

                            <div class="form-group">
                                <label >Email</label>
                                <input  type="email" class="form-control dark" name="mail">
                            </div>

                            <div class="form-group">
                                <label>Mot de passe</label>
                                <input  type="password" class="form-control dark" name="mdp">
                            </div>

                            <div class="form-group">
                                <label>Confirmation de mot de passe</label>
                                <input  type="password" class="form-control dark" name="mdp2">
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn free-access form-group"
                                    name="forminscription_ens">
                                Valider
                            </button>

                            <button type="submit" class="btn cancel form-group"
                                    name="">
                                Fermer
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="module hide" id="formulaire-entreprise">
        <div class="row" id="ancre_connexion_entreprise">
            <div class="col-md-4 col-md-offset-4">
                <div class="text-center">
                    <button type="button" class="btn btn-connexion-entreprise"
                        onclick="afficherConnexionEntreprise()" aria-label="Connexion">
                        Connexion
                    </button>

                    <button type="button" class="btn btn-inscription-entreprise"
                            aria-label="Inscription"
                            onclick="afficherInscriptionEntreprise()">
                        Inscription
                    </button>
                </div>           
            </div>         
        </div>

        <div class="row mt-50">
            <div class="col-md-4 col-md-offset-4 arounder ">
                <!-- connexion entreprise -->
                <!-- connexion entreprise --> 
                <div id="connexion-entreprise">            
                    <form action="" method="POST" role="form">
                        <div class="form-group">
                            <!-- Email -->
                            <label>Email</label>
                            <input  type="email" class="form-control dark" name="mailconnect">       
                        </div>

                        <!-- Mot de passe -->
                        <div class="form-group">
                            <label>Mot de passe</label>
                            <input  type="password" class="form-control dark" name="mdpconnect" id="pwd-entreprise-saisi">      
                        </div>

                        <div class="form-check">
                            <input type='checkbox' >
                            <small class="form-check-label">Afficher le mot de passe</small>
                        </div>

                        <div class="text-right form-group mb-2em hover-me" onclick="ouvrirRecuperationPassword()">
                            <small>Mot de passe oublié ?</small>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn free-access form-group"
                                    name="formconnexion" >
                                Se connecter
                            </button>

                            <button type="submit" class="btn cancel form-group"
                                    name="">
                                Fermer
                            </button>
                        </div>
                    </form>
                </div>
                <!-- inscription entreprise -->
                <!-- inscription entreprise -->
                <div class="hide" id="inscription-entreprise">
                    <form action="" method="POST" role="form" class="inscription-block">
                        <div class="mb-4em">
                            <div class="form-group">
                                <label>Nom</label>
                                <input  type="text" class="form-control dark" name="nom">
                            </div>

                            <div class="form-group">
                                <label for="prenom">Prénom</label>
                                <input  type="text" class="form-control dark" name="prenom" id="prenom">
                            </div>

                            <div class="form-group">
                                <label for="raisonsocial"> Raison sociale Entreprise </label>
                                <input  type="text" class="form-control dark" name="raisonsocial" id="raisonsocial">
                            </div>

                            <div class="form-group">
                                <label for="tel"> Numéro de téléphone </label>
                                <input  type="text" class="form-control dark" name="tel" id="tel">
                            </div>

                            <div class="form-group">
                                <label for="mail">Email</label>
                                <input  type="email" class="form-control dark" name="mail" id="mail">
                            </div>

                            <div class="form-group">
                                <label for="mdp">Mot de passe</label>
                                <input  type="password" class="form-control dark" name="mdp" id="mdp">
                            </div>

                            <div class="form-group">
                                <label for="mdp2">Confirmation de mot de passe</label>
                                <input  type="password" class="form-control dark" name="mdp2" id="mdp2">
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn free-access form-group"
                                    name="forminscription_ent">
                                Valider
                            </button>

                            <button type="submit" class="btn cancel form-group"
                                    name="">
                                Fermer
                            </button>
                        </div>
                    </form>
                </div> 
            </div>
        </div>
    </section>
</div>











