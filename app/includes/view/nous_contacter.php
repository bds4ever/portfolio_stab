<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h2 class="module-subtitle font-alt">Nous contacter!</h2>
        </div>
    </div>

    <div class="row wow fadeInUp" data-wow-delay="0.6s">
        <div class="col-sm-8 col-sm-offset-2">
           <p class="text-justify">
                Nous sommes joignable au <strong>+33 (0)3 22 82 72 72</strong> ou au '<strong>9</strong>' en interne. Le standard de l'université reste ouvert du lundi au vendredi de 8h00 à 12h30 et de 13h30 à 17h30. Adressez nous des courriers à l'adresse <strong>Université de Picardie Jules Verne - Chemin du Thil - CS 52501 - 80025 Amiens Cedex 1</strong>. Vous pouvez également nous écrire directement en cliquant sur le bouton ci-dessous.
            </p>

            <div class="text-center mt-50 mb-50">
                <a class="btn free-access form-group section-scroll" role="button" 
                   href="#formulaire-contact" onclick="voirFormulaireContact()">
                    Nous écrire
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="hide" id="formulaire-contact">
                <form>
                    <div class="form-group">
                        <label>Nom</label>
                        <input type="text" class="form-control dark" name="nom">
                    </div>

                    <div class="form-group">
                        <label>Téléphone</label>
                        <input type="tel" class="form-control dark " name="telephone">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control dark" name="email">
                    </div>

                    <div class="form-group">
                        <label>Votre message</label>
                        <textarea rows="4" class="form-control dark" name="comment" form="usrform"></textarea>
                    </div>

                    <div class="text-center mt-50">
                        <button type="submit" value="Envoyer" class="btn free-access">
                            Envoyer
                        </button>
                    </div>
                </form>
            </div>
        </div>         
    </div>
</div>