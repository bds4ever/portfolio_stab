<div class="container">             
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <strong class="holder-w"></strong>
            <h2 class="module-subtitle font-alt"> A propos </h2> 
            <strong class="holder-w"></strong>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 wow zoomIn">
            <p class="text-justify">
                <strong>Digifolio-upjv</strong> est une plateforme permettant aux étudiants de constituer un dossier personnel structuré et alimenté au cours d'un apprentissage ou d'une formation. Ce dossier témoigne des activités et productions réalisées, des documents consultés ou encore des compétences qu'ils ont acquises.
            </p>

            <p class="text-justify">
                Les ePortfolios des étudiants étant diffusés via Internet et les réseaux sociaux, ils peuvent    contribuer indirectement à <strong>diffuser de la culture et l’information scientifique et technique</strong>,  notamment au cours du master, assurant ainsi une meilleure visibilité des diplômes et unités de  formation. Ils peuvent également de la même manière <strong>diffuser les résultats de la recherche scientifique et technologique</strong>, en particulier au cours du doctorat, assurant une meilleure   visibilité des laboratoires et unités de recherche.
            </p>

            <p class="text-justify">
                Enfin, en permettant une meilleure visibilité des acquis de la formation ainsi que  de la maturité académique et professionnelle des étudiants, la démarche ePortfolio favorise la mobilité des étudiants et développe la <strong>notoriété des établissements au-delà des frontières</strong>, contribuant ainsi   à la construction de l’Espace européen de l’enseignement supérieur et de la recherche. On peut faire l’hypothèse qu’elle peut ainsi, à terme,  offrir de nouvelles opportunités    en  matière de coopération internationale.
            </p>

            <p>
                <strong>Cette version béta de l'application a été développée par Bilal AKAR et Ben da SILVEIRA, tous deux, étudiants en 2ème année de Master Miage, parcours 2 Com.</strong>
            </p>
        </div>                           
    </div>
</div>
