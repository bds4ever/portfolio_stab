<?php

session_start();

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_portfolio;charset=utf8', 'root', '');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
}

if(isset($_POST['id_domaine'])){
    $id_domaine = intval($_POST['id_domaine']);

    $req = "SELECT DISTINCT ne.* FROM niveau_etude ne 
            INNER JOIN domaine_niveau_mention dnm 
            ON dnm.id_niveau_etude = ne.id
            AND dnm.id_domaine = ?
            ORDER BY ne.titre ASC";

    $req_niveau_etudes = $bdd->prepare($req);
    $req_niveau_etudes->execute(array($id_domaine));
    $niveau_etudes = $req_niveau_etudes->fetchAll();

    //die(var_dump($niveau_etudes));
    echo json_encode($niveau_etudes);
    return;
}
?>


<!--<label>Niveau d'études</label>
<select class="form-control" id="slct1" name="slct1" onchange="voirMention(event)">
    <option value="selectionNiveauEtudes" disabled selected>Sélectionnez</option>
    <?php foreach ($niveau_etudes as $niveau): ?>
        <option value="<?= $niveau['id'] ?>"><?= $niveau['titre'] ?></option>
    <?php endforeach; ?>
</select>-->

