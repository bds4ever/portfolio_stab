<div class="container">             
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <strong class="holder-w"></strong>
            <h2 class="module-subtitle font-alt"> L'equipe projet</h2> 
            <strong class="holder-w"></strong>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="row mb-50">          
                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="text-center mb-20">
                        <img src="./img/equipe_projet/bds3.jpg" style="border-radius:50%; width: 150px; height: 150px;box-shadow: 1px 1px 12px #555;">
                    </div>
                    
                    <p class="text-center mb-20">
                        Ben da SILVEIRA<br><strong> Développeur front-end</strong><br>
                        <strong>Chef de projet junior</strong>
                    </p>
                    <p>
                        Ben est en Master MIAGE, parcours <strong>Conduite de Projet Multimédia</strong>. Son actuelle spécialisation est le développement front-end. Il travaille sur des technologies comme Jquery ou encore AngularJs, voire Angular.
                    </p>
                </div>

                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="text-center mb-20">
                        <img src="./img/equipe_projet/bilal.jpg" style="border-radius:50%; width: 150px; height: 150px;box-shadow: 1px 1px 12px #555;">
                    </div>

                    <p class="text-center mb-20">Bilal AKAR <br><strong>Développeur web</strong></p>
                    <p>
                        Bilal est en Master MIAGE, parcours <strong>Conduite de Projet Multimédia</strong>. Sur ce projet, il est intervenu en tant que développeur back-end. Il a aussi étroitement participé à toutes les autres tâches transverses comme la rédaction des spécifications fonctionctionnelles détaillées. 
                    </p>
                </div>
            </div>
            <!-- bloc 2 -->
            <div class="row">          
                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="text-center mb-20">
                        <img src="./img/equipe_projet/catherine_b.jpg" style="border-radius:50%; width: 150px; height: 150px;box-shadow: 1px 1px 12px #555;">
                    </div>

                    <p class="text-center mb-20">Catherine BARRY <br> <strong>Maître de conférences</strong></p>
                    
                    <p>
                        Afin de mettre en situation réelle leurs étudiants en fin de cycle, madame BARRY fait partie des clients proposant une solution à développer. C'est elle qui a défini les principales fonctionnalités de cette application. 
                    </p>
                </div>

                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="text-center mb-20">
                        <img src="./img/equipe_projet/profil.jpg" style="border-radius:50%; width: 150px; height: 150px;box-shadow: 1px 1px 12px #555;">
                    </div>
                    <p class="text-center mb-20">Jean-Luc GUERIN <br><strong>Professeur d'Université</strong></p>
                    <p>
                        Monsieur Jean-Luc GUERIN est la personne chargée de suivre l'évolution de Ben et Bilal dans la gestion de leur projet. Il s'assure que le binôme respecte la méthodologie de gestion de projet qu'il a défini.
                        Etant parti sur SCRUM, il incarne plus ou moins le rôle de scrum master.
                    </p>
                </div>
            </div>
        </div>                             
    </div>
</div>
