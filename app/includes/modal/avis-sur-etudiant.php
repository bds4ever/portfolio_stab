<?php

?>
<div class="modal fade" id="avis-sur-etudiant-dialog" 
    tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
          <!-- contenu de la modal -->
            <div class="modal-body">
                <p class="text-justify">Vous avez vu évoluer l'étudiant à l'université ou en entreprise.</p>
                <p class="font-weight-bold text-center">Votre appréciation nous intéresse !</p>
            </div>

            <div class="modal-footer">
                <button type="button"
                        class="btn cancel min"
                        data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>
