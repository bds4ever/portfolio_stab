<?php

?>
<div class="modal fade" id="confirmation-deconnexion" 
    tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
          <!-- contenu de la modal -->
            <div class="modal-body">
                <p class="text-center">
                    Etes-vous sûr de vouloir vous déconnecter?
                </p>
            </div>

            <div class="modal-footer text-center">
                <button type="button" class="btn validation min" data-dismiss="modal">
                    Oui
                </button>

                <button type="button" class="btn cancel min" data-dismiss="modal">             
                    Non
                </button>
            </div>
        </div>
    </div>
</div>
