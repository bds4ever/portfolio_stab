<div class="modal fade copyright"
    tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Copyright</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
          <!-- contenu de la modal -->
            <div class="modal-body">
                <p class="text-justify">
                    Ce site est une propriété...
                </p>
            </div>

            <div class="modal-footer">
                <button type="button"
                        class="btn cancel"
                        data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>
