<?php

?>
<div class="modal fade dev-to-come-up"
    tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
          <!-- contenu de la modal -->
            <div class="modal-body">
                <p>
                    <i class="fas fa-info-circle"></i>
                    &nbsp;Attention, ceci est une fonctionnalité à venir !!!
                </p>
            </div>

            <div class="modal-footer">
                <button type="button"
                        class="btn cancel min"
                        data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>
