<?php

?>
<div class="modal fade forgotten-password"
    tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Récupération de compte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
            <!-- contenu de la modal -->
            <div class="modal-body">
                <p class="text-center">
                    Indiquez l'adresse mail du compte que vous souhaitez récupérer
                </p>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email">
                </div>
            </div>

            <div class="text-center form-group">
                <button type="button" class="btn validation min" data-dismiss="modal">
                    Valider
                </button>

                <button type="button" class="btn cancel min" data-dismiss="modal">
                    Fermer
                </button>
            </div>
        </div>
    </div>
</div>
