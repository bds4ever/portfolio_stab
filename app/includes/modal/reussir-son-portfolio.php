<div class="modal fade reussir-son-portfolio"
    tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Réussir son portfolio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="text-white">&times;</span>
                </button>
            </div>
           <!-- contenu de la modal -->
            <div class="modal-body">
                <p class="text-justify">
                    Vous êtes sur le point de créer <strong>créer votre portfolio</strong>, vous savez donc que ça vous apportera une plus value sur le marché de l'emploi. Créer son dossier personnel de compétences n'est donc pas un exercice à prendre à la légère.<strong>Cela impose une rigueur dans son élaboration</strong>. 
                </p>

                <p class="text-justify">
                    Avant de vous lancer, <strong>il impoprte que vous vous posiez les bonnes questions</strong>. Quel est votre projet professionnel ? Qu'attend un employeur de vous ? Est-ce que toutes les informations sont bonnes à donner ? Dans le doute, inspirez vous de ce qui se fait de mieux autour de vous. <strong>Don't stress! Vous serez guidé dans chacune des sections de cette application</strong>.
                </p>

                <p class="text-justify">
                    Vous serez tout naturellement appelé à renseigner vos <strong>expériences professionnelles</strong>. Pour ce fait, nous nous sommes appuyer sur le <strong>référentiel européen des compétences</strong>, pour une première mise en production de l'application. Vous pouvez <strong>télécharger ce document à l'accueil de votre espace</strong>.
                </p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn cancel" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
